using OpenArtVirtualGallery.Gallery;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Installers
{
    public class ProjectContextInstaller : MonoInstaller
    {
        [SerializeField] private DebugLog _debugLog;

        public override void InstallBindings()
        {
            Container.Bind<ScenesManager>().AsSingle().NonLazy();
            Container.Bind<AppSetup>().AsSingle().NonLazy();

            var localUser = new VisitorHeroModel(true);
            Container.Bind<UserActions>().AsSingle();

            Container.BindInterfacesAndSelfTo<VisitorsManager>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<LangSelector>()
                .AsSingle();
        
            Container.Bind<GalleryActions>()
                .AsSingle();

            Container.BindInstance(localUser).AsSingle();
        
            Container.Bind<LocalUserHeroResolver>()
                .AsSingle()
                .WithArguments(localUser);
        
            Container.BindInstance(_debugLog).AsSingle();
        }
    }
}
