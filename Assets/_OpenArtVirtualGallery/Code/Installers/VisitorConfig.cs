using OpenArtVirtualGallery.Visitor;
using OpenArtVirtualGallery.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

[CreateAssetMenu(fileName = "HeroConfig", menuName = "Installers/HeroConfig")]
public class VisitorConfig : ScriptableObjectInstaller<VisitorConfig>
{
    [PreviewField(ObjectFieldAlignment.Left, Height = 200)]
    [SerializeField] private Sprite _avatarPicture;

    [Space(10)]
    [Tooltip("В случае если это персонаж игрока")]
    [SerializeField] private Vector3 _cameraPosition;
    [SerializeField] private Vector3 _nicknameDisplayPosition;
    
    [Space(10)]
    [SerializeField] private BodyController _heroModelPrefabRef;
    
    [Space(5)]
    [SerializeField] private Color _color;
    
    [Space(10)]
    [SerializeField] private RuntimeAnimatorController _animator;
    [SerializeField] private Avatar _animatorAvatar;

    public Sprite AvatarPicture => _avatarPicture;
    public Vector3 CameraPosition => _cameraPosition;
    public Vector3 NicknameDisplayPosition => _nicknameDisplayPosition;
    public BodyController HeroModelPrefabRef => _heroModelPrefabRef;
    public Color Color => _color;
    public RuntimeAnimatorController Animator => _animator;
    public Avatar AnimatorAvatar => _animatorAvatar;
    
    public override void InstallBindings() { }
}