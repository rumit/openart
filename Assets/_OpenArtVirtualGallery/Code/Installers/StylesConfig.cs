using System;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "StylesConfig", menuName = "Installers/StylesConfig")]
public class StylesConfig : ScriptableObjectInstaller<StylesConfig>
{
    [SerializeField] private BaseColors _baseColors;
    
    public override void InstallBindings()
    {
        Container.BindInstance(_baseColors).AsSingle();
    }

    [Serializable]public class BaseColors
    {
        [SerializeField] private Color _bgroundColor;
        [SerializeField] private Color _colorFirst;
        [SerializeField] private Color _redColor;
        
        [Space(10)]
        [SerializeField] private Color _avatarPreviewFrameColor;
        [SerializeField] private Color _avatarPreviewSelectedFrameColor;
        
        public Color GetBgroundColor() => _colorFirst;
        public Color GetFirstColor() => _bgroundColor;
        public Color GetGetColor() => _redColor;
        public Color GetAvatarPreviewFrameColor() => _avatarPreviewFrameColor;
        public Color GetAvatarPreviewSelectedFrameColor() => _avatarPreviewSelectedFrameColor;
    }
}