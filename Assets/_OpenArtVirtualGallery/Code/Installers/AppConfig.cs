using System;
using System.Collections.Generic;
using AYellowpaper.SerializedCollections;
using OpenArtVirtualGallery;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using OpenArtVirtualGallery.Visitor;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "AppConfig", menuName = "Installers/AppConfig")]
public sealed class AppConfig : ScriptableObjectInstaller<AppConfig>
{
    public enum Layers
    {
        TeleportEntry = 3,
        Floor = 6,
        PlayerBody = 7,
        Visitor = 9
    }

    [Space]
    [SerializeField] private Common _commonConfig;
    [SerializeField] private LangConfig _langConfig;
    [SerializeField] private HeroConfigsList _heroConfigsList;
    [SerializeField] private VisitorPrefabs _visitorPrefabs;
    [SerializeField] private CommonAppPrefabs _commonAppPrefabs;
    [SerializeField] private PublishSettings _publishSettings;
    [SerializeField] private URLs _urls;

    public override void InstallBindings()
    {
        Container.BindInstance(_commonConfig).AsSingle();
        Container.BindInstance(_heroConfigsList).AsSingle();
        Container.BindInstance(_commonAppPrefabs).AsSingle();
        Container.BindInstance(_visitorPrefabs).AsSingle();
        Container.BindInstance(_langConfig).AsSingle();
        Container.BindInstance(_publishSettings).AsSingle();
        Container.BindInstance(_urls).AsSingle();
    }

    [Serializable] 
    public sealed class Common
    {
        [SerializeField] private SerializedDictionary<ScenesManager.Scenes, string> _exibitionNames;
        public string GetExibitionName(ScenesManager.Scenes scene) => _exibitionNames[scene];
    }

    [Serializable] 
    public sealed class URLs
    {
        [SerializeField] private string _telegramChatUrl = "https://t.me/galleryOpenArt";
        public string TelegramChatUrl => _telegramChatUrl;
        
        [SerializeField] private string _videosUrl = "http://test.openart.moscow/virtual/2022/videos/";
        public string VideosUrl => _videosUrl;
    }
    
    [Serializable]
    public struct LangConfig
    {
        [SerializeField] private LangSelector.Langs _defaultLang;
        public LangSelector.Langs DefaultLang => _defaultLang;
    }

    [Serializable]
    public sealed class HeroConfigsList
    {
        [SerializeField] private List<VisitorConfig> _configs;
        public List<VisitorConfig> Get() => _configs;
        public VisitorConfig GetConfig(int index) => _configs[index];
    }
    
    [Serializable] 
    public sealed class VisitorPrefabs
    {
        [SerializeField] private VisitorController _visitorPrefabRef;
        [SerializeField] private BotController _botVisitorRef;
        public VisitorController GetHeroPrefab() => _visitorPrefabRef;
        public BotController GetBotVisitorPrefab() => _botVisitorRef;
    }
    
    [Serializable] 
    public sealed class CommonAppPrefabs
    {
        [SerializeField] private ModalPopup _notificationPopupPrefabRef;
        [SerializeField] private ModalPopup _dialogPopupPrefabRef;
        public ModalPopup GetNotificationPopupPrefab() => _notificationPopupPrefabRef;
        public ModalPopup GetDialogPopupPrefab() => _dialogPopupPrefabRef;
    }
    
    [Serializable] 
    public sealed class PublishSettings
    {
        [SerializeField] private bool _isDebug;
        public bool IsDebug => _isDebug;
    }
}