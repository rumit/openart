﻿using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Gallery.Exhibitors;
using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Installers
{
    /// <summary>
    /// Инсталлер объектов общего для всех выставок контекста
    /// </summary>
    public class GallerySceneContextInstaller :  MonoInstaller
    {
        [SerializeField] private ExhibitionsSceneConfig _sceneConfig;

        public override void InstallBindings()
        {
            Container.Bind<MediaEvents>().AsSingle();

            Container.BindInstance(_sceneConfig).AsSingle();
            
            Container.BindInterfacesAndSelfTo<UserTeleportsManager>()
                .AsSingle()
                .NonLazy();
        }
    }
}