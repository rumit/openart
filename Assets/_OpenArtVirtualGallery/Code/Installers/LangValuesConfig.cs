﻿using System;
using System.Collections.Generic;
using OpenArtVirtualGallery.User;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using Zenject;

public abstract class LangValuesConfig<T> : ScriptableObjectInstaller<AppConfig> where T : Enum
{
    [SerializeField]protected LangValuesData _langValues;

    public override void InstallBindings()
    {
        Container.BindInstance(_langValues);
        Container.Bind<LangValues>()
            .AsCached()
            .WithArguments(_langValues);
    }

    public class LangValues
    {
        [Inject] private LangSelector _langSelector;

        private readonly List<string> _additionalParams = new() { "{0}", "{1}", "{2}" };
        private readonly LangValuesData _config;

        public LangValues(LangValuesData config) => _config = config;

        public string Get(T key, params string[] addParams)
        {
            var value = _config.Hash[key];

            if (addParams.IsNullOrEmpty())
            {
                return _langSelector.Current == LangSelector.Langs.En ? value._eng : value._ru;
            }

            var mess = _langSelector.Current == LangSelector.Langs.En ? value._eng : value._ru;
            for (var i = 0; i < _additionalParams.Count; i++)
            {
                var element = _additionalParams[i];
                if(i >= addParams.Length)
                    continue;
                mess = mess.Replace(element, addParams[i]);
            }

            return mess;
        }
    }

    [Serializable]
    public class LangValuesData
    {
        [SerializeField]protected SerializedDictionary<T, LangValueData> _hash;
        public SerializedDictionary<T, LangValueData> Hash => _hash;
    }
}

[Serializable]
public class LangValueData : ILangVaueData
{
    [SerializeField][TextArea(1, 500)] public string _eng;
    [SerializeField][TextArea(1, 500)] public string _ru;
    
    public string Eng => _eng;
    public string Ru => _ru;
}

public interface ILangVaueData
{
    string Eng { get; }
    string Ru { get; }
}
