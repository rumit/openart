﻿using UnityEngine;

[CreateAssetMenu(fileName = "LangBaseValuesConfig", menuName = "Installers/LangBaseValuesConfig")]
public class LangBaseValuesConfig : LangValuesConfig<LangBaseValuesConfig.BaseLangIds>
{
    public enum BaseLangIds
    {
        VirtualGalleryTitle,
        Enter,
        EnterYouName,
        Hi,
        SelectAvatar,
        OpenChat,
        CloseChat,
        Send,
        Yes,
        No,
        GoUp,
        GoDown,
        ServerDisconnect,
        ExhibitionEntranceMessage,
        ExhibitionContinuationMessage
    }
}
