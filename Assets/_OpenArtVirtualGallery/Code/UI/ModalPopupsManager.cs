﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;

public static class ModalPopupsManager
{
    public static IPopup Current { get; private set; }
    
    public static void ShowPopup(IPopup popupPrefab, Transform root)
    {
        ShowPopupInternal(popupPrefab, root);
        Current.Show();
    }
    
    public static void ShowPopup(IPopup popupPrefab, Transform root, string message)
    {
        ShowPopupInternal(popupPrefab, root);
        Current.Show(message);
    }

    private static async void ShowPopupInternal(IPopup popupPrefab, Transform root)
    {
        if (Current != null)
            await RemovePopup();

        if (popupPrefab.gameObject.GetComponent<IPopup>() == null)
            throw new Exception("The popup prefab must have a script that implements the IPopup interface");
        
        Current = Object.Instantiate(popupPrefab.gameObject, root).GetComponent<IPopup>();
        Current.Window.localScale = Vector3.zero;
        Current.Window.DOScale(Vector3.one, .3f).SetEase(Ease.OutBack);
    }

    public static async UniTask RemovePopup()
    {
        if(Current == null)
            return;

        await Current.Window.DOScale(Vector3.zero, .2f)
            .SetEase(Ease.OutSine)
            .AsyncWaitForCompletion();
        
        Object.Destroy(Current.gameObject);
        Current = null;
    }
}