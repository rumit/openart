﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.UI
{
    public class SceneLoadProgressbarUI : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _view;
        [SerializeField] private TextMeshProUGUI _progressField;
        [SerializeField] private Image _progressCircle;
        
        private ScenesManager _scenesManager;
        private GameObject _progressbarContainer;

        [Inject]
        private void Inject(ScenesManager scenesManager)
        {
            _scenesManager = scenesManager;
        }

        private void OnDestroy()
        {
            _scenesManager.SceneLoadStart -= OnSceneLoadStart;
            _scenesManager.SceneLoadProgress -= OnSceneLoadProgress;
            _scenesManager.SceneLoaded -= OnSceneLoaded;
        }

        private void Awake()
        {
            _view.gameObject.SetActive(false);

            _progressbarContainer = _progressCircle.transform.parent.gameObject;

            _scenesManager.SceneLoadStart += OnSceneLoadStart;
            _scenesManager.SceneLoadProgress += OnSceneLoadProgress;
            _scenesManager.SceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoadStart()
        {
            _progressbarContainer.gameObject.SetActive(true);
            _progressCircle.gameObject.SetActive(true);
            _progressField.text = "";
            _progressCircle.fillAmount = 0;

            _progressCircle.gameObject.SetActive(true);
            _view.gameObject.SetActive(true);
            _view.alpha = 0;
            _view.DOFade(1, .8f);
        }
        
        private void OnSceneLoadProgress(float progress)
        {
            var roundedProgress = (int) (progress * 100);
            if (roundedProgress >= 100)
            {
                _progressField.text = "initializing...";
                _progressCircle.gameObject.SetActive(false);
                return;
            }
            _progressField.text = $"{roundedProgress.ToString()}%";
            _progressCircle.fillAmount = progress;
        }

        private async void OnSceneLoaded()
        {
            _progressbarContainer.gameObject.SetActive(false);
            
            await _view.DOFade(0, .8f).AsyncWaitForCompletion();
            _view.gameObject.SetActive(false);
        }
    }
}