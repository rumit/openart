﻿using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static LangBaseValuesConfig;

[RequireComponent(typeof(ZenAutoInjecter))]
public class ModalPopup : SerializedMonoBehaviour, IPopup
{
    public event Action<bool, IPopup> ActionSelected;
    
    [SerializeField] private Transform _window;
    [SerializeField] private Button _okButton;
    [SerializeField] private Button _noButton;
    [SerializeField] private TextMeshProUGUI _messageField;

    public Transform Window => _window;
    private LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;

    [Inject]
    private void Inject(LangValuesConfig<BaseLangIds>.LangValues langBaseValues)
    {
        _langBaseValues = langBaseValues;
    }
    
    private void OnDestroy()
    {
        _okButton.onClick.RemoveListener(OnOkButtonClick);
        if(_noButton != null)
            _noButton.onClick.RemoveListener(OnNoButtonClick);
    }

    private void Start()
    {
        _okButton.GetComponentInChildren<TextMeshProUGUI>().text = _langBaseValues.Get(BaseLangIds.Yes);
        if(_noButton != null)
            _noButton.GetComponentInChildren<TextMeshProUGUI>().text = _langBaseValues.Get(BaseLangIds.No);
    }

    public void Show(string message)
    {
        ShowInternal();
        _messageField.text = message;
    }
    
    public void Show() => ShowInternal();

    private void ShowInternal()
    {
        _okButton.onClick.AddListener(OnOkButtonClick);
        if(_noButton != null)
            _noButton.onClick.AddListener(OnNoButtonClick);
    }

    private void OnOkButtonClick() => ActionSelected?.Invoke(true, this);
    
    private void OnNoButtonClick() => ActionSelected?.Invoke(false, this);
}
