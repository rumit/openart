﻿using System;

namespace OpenArtVirtualGallery.UI.Items
{
    public interface IItem<T>
    {
        void Select();
        void Deselect();
        T Data { get; }
    }
}