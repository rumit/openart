﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace OpenArtVirtualGallery.UI.Items
{
    public abstract class ItemsGroup<T> : SerializedMonoBehaviour
    {
        [NonSerialized]public Action<IItem<T>> ItemTapped;

        [SerializeField] protected IItem<T>[] _items;
        [Space(5)]
        [SerializeField] protected Color _selectedItemColor;

        public IItem<T> SelectedItem { get; protected set; }
        public Color SelectedItemColor => _selectedItemColor;

        protected virtual void Start()
        {
            ItemTapped += OnItemTapped;
        }

        protected void OnDestroy()
        {
            ItemTapped -= OnItemTapped;
        }

        protected virtual void SelectItem(IItem<T> item)
        {
            item.Select();
            SelectedItem = item;
        }

        protected void OnItemTapped(IItem<T> item)
        {
            if (SelectedItem == null)
            {
                SelectItem(item);
                return;
            }

            if (item == SelectedItem)
                return;
            
            SelectedItem.Deselect();
            SelectItem(item);
        }
    }
}