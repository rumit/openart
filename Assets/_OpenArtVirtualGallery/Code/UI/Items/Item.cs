﻿using UnityEngine;
using UnityEngine.UI;

namespace OpenArtVirtualGallery.UI.Items
{
    [RequireComponent(typeof(Button), typeof(Graphic))]
    public abstract class Item<T> : MonoBehaviour, IItem<T> where T : IItemDataProvider
    {
        [SerializeField] protected ItemsGroup<T> _itemsGroup;
        [SerializeField] protected T _data;
        
        public T Data => _data;

        protected Color _defaultColor;
        protected Button _button;
        protected Graphic _graphic;

        protected void Awake()
        {
            _button = GetComponent<Button>();
            _graphic = GetComponent<Graphic>();
            _defaultColor = _graphic.color;
        }

        protected void Start()
        {
            _button.onClick.AddListener(OnClick);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        protected void OnClick()
        {
            _itemsGroup.ItemTapped?.Invoke(this);
        }

        public abstract void Select();

        public abstract void Deselect();
    }

    public interface IItemDataProvider { }
}