﻿using System;
using UnityEngine;

public interface IPopup
{
    public event Action<bool, IPopup> ActionSelected;
    
    void Show(string message);

    void Show();
    
    GameObject gameObject { get; }
    Transform Window { get; }
}
