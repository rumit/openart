﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace OpenArtVirtualGallery.UI
{
    public class RuntimeStartSceneCaller : MonoBehaviour
    {
        private void Awake()
        {
            var currScene = SceneManager.GetActiveScene();
            if (currScene.buildIndex != 0)
                SceneManager.LoadScene(0);
        }
    }
}