﻿using System;
using TMPro;
using UnityEngine;

public class DebugLog : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textField;

    private string _currentString = "";

    private void Awake()
    {
        if (SystemInfo.deviceName == "TEMA-DESKTOP")
            Show();
        else
            Hide();
    }

    public void Show() => _textField.gameObject.SetActive(true);
    public void Hide() => _textField.gameObject.SetActive(false);
    
    public void Print(object message)
    {
        var d = DateTime.Now;
        var t = $"{d.Minute:00}:{d.Second:00}:{d.Millisecond:00}";
        _textField.text = _currentString.Length > 0 ? $"{_currentString}\n{t} {message}" : $"{t} {message}";
        _currentString = _textField.text;
    }

    public void Clear()
    {
        _textField.text = _currentString = "";
    }
}
