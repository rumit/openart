﻿using DG.Tweening;
using UnityEngine.UI;
using Logger = OpenArtVirtualGallery.Utils.Logger;

public class TwoStatesButton : Toggle
{
    private Image _baseStateImage;

    protected override void OnDestroy()
    {
        base.OnDestroy();
        onValueChanged.RemoveListener(OnValueChanged);
    }

    protected override void Awake()
    {
        base.Awake();
        onValueChanged.AddListener(OnValueChanged);
        _baseStateImage = graphic.transform.parent.gameObject.GetComponent<Image>();
    }

    // protected override void Start()
    // {
    //     base.Start();
    // }

    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        base.DoStateTransition(state, instant);
        graphic.color = !interactable ? ColorBlock.defaultColorBlock.disabledColor : ColorBlock.defaultColorBlock.normalColor;
    }

    // public void SetOn()
    // {
    //     isOn = true;
    //     _baseStateImage.enabled = false;
    //     //OnValueChanged(true);
    // }

    private void OnValueChanged(bool value)
    {
       // Logger.Log($"OnValueChanged {value}");
        _baseStateImage.enabled = !isOn;
    }
}
