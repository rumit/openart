﻿using ExitGames.Client.Photon;
using OpenArtVirtualGallery.Authorization.UI;
using OpenArtVirtualGallery.User;
using Photon.Pun;
using UnityEngine;
using Zenject;
using Newtonsoft.Json;
using OpenArtVirtualGallery.Authorization.UI.OptionSelection;

namespace OpenArtVirtualGallery.Authorization
{
    public class AuthorizationController : MonoBehaviourPunCallbacks
    {
        [SerializeField] private AuthorizationUI _authorizationUi;
        [SerializeField] private OptionsSelectionUI _optionsSelectionUI;
        
        private LocalUserHeroResolver _localUserHeroResolver;
        private VisitorHeroModel _localUser;
        private JoinRoomController _joinRoomController;

        [Inject]
        private void Inject(JoinRoomController joinRoomController, LocalUserHeroResolver localUserHeroResolver)
        {
            _joinRoomController = joinRoomController;
            _localUserHeroResolver = localUserHeroResolver;
        }

        private void Awake()
        {
            _authorizationUi.gameObject.SetActive(true);
            _authorizationUi.ConnectToServerRequest += OnConnectToServerRequest;

            _optionsSelectionUI.JoinRoomRequest += OnJoinRoomRequest;
            _optionsSelectionUI.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _optionsSelectionUI.JoinRoomRequest -= OnJoinRoomRequest;
            _authorizationUi.ConnectToServerRequest -= OnConnectToServerRequest;
        }

        private void OnConnectToServerRequest(string userName)
        {
            PhotonNetwork.NickName = userName;
            Debug.Log($"{PhotonNetwork.NickName} connecting...");
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            _localUser = _localUserHeroResolver.Get();
            _localUser.Name = PhotonNetwork.NickName;
            _localUser.PhotonUserId = PhotonNetwork.LocalPlayer.UserId;
            
            _authorizationUi.gameObject.SetActive(false);
            _optionsSelectionUI.gameObject.SetActive(true);
            
            Debug.Log($"{PhotonNetwork.NickName} connected to master");
            _optionsSelectionUI.Init(_localUser);
        }

        private void OnJoinRoomRequest(string scene)
        {
            var hash = new Hashtable
            {
                {_localUser.PhotonUserId, JsonConvert.SerializeObject(_localUser)}
            };
            PhotonNetwork.SetPlayerCustomProperties(hash);
            
            _joinRoomController.JoinRoom(scene);
        }
    }
}