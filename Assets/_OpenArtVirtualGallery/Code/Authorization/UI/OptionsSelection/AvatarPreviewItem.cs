﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
    public class AvatarPreviewItem : MonoBehaviour
    {
        [SerializeField] private Image _pictureHolder;
        
        private AvatarPreviewInteractor _interactor;

        public int Index { get; private set; }

        public void Init(UserAvatarSelector avatarsSelector, Sprite picture, int index)
        {
            _interactor = GetComponentInChildren<AvatarPreviewInteractor>();
            _pictureHolder.sprite = picture;
            Index = index;
            _interactor.Init(this, avatarsSelector);
        }

        public void Select() => _interactor.Select();

        public void Deselect() => _interactor.Deselect();
    }
}