﻿using OpenArtVirtualGallery.UI.Items;
using TMPro;
using UnityEngine;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
    public sealed class ExhibitionSelectorItem : Item<ExhibitionSelectorsGroup.SelectorData>
    {
        [SerializeField] private TextMeshProUGUI _textField;
        
        public override void Select()
        {
            _textField.color = _itemsGroup.SelectedItemColor;
        }

        public override void Deselect()
        {
            _textField.color = _defaultColor;
        }
    }
}