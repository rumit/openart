﻿using System;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static LangBaseValuesConfig;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
     public sealed class OptionsSelectionUI : MonoBehaviour
     {
         public event Action<string> JoinRoomRequest;
         
         [SerializeField] private TextMeshProUGUI _greetingDisplay;
         [SerializeField] private TextMeshProUGUI _selectAvatarField;
         [SerializeField] private UserAvatarSelector _avatarSelector;
         [SerializeField] private ExhibitionSelectorsGroup _exhibitionsSelector;
         [SerializeField] private Button _enterButton;

         private LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;
         private VisitorHeroModel _localUser;

         [Inject]
         private void Inject(LangValuesConfig<BaseLangIds>.LangValues langBaseValues)
         {
             _langBaseValues = langBaseValues;
         }

         private void OnDestroy()
         {
             _avatarSelector.ItemSelected -= OnAvatarSelectorItemSelected;
             _enterButton.onClick.RemoveListener(OnEnterButtonClick);
         }

         private void Awake()
         {
             _enterButton.interactable = false;
             _enterButton.onClick.AddListener(OnEnterButtonClick);
             
             _avatarSelector.ItemSelected += OnAvatarSelectorItemSelected;
             _avatarSelector.gameObject.SetActive(false);
         }
         
         public void Init(VisitorHeroModel localUser)
         {
             _localUser = localUser;
             _greetingDisplay.text = $"{_langBaseValues.Get(BaseLangIds.Hi)}, {localUser.Name}!";
             _selectAvatarField.text = _langBaseValues.Get(BaseLangIds.SelectAvatar);
             _enterButton.GetComponentInChildren<TextMeshProUGUI>().text = _langBaseValues.Get(BaseLangIds.Enter);

             _avatarSelector.Init();
         }

         private void OnEnterButtonClick()
         {
             _enterButton.onClick.RemoveListener(OnEnterButtonClick);
             
             _localUser.Index = _avatarSelector.SelectedItem.Index;
             Debug.Log($"OnEnterButtonClick >>> exhibition: {_exhibitionsSelector.SelectedItem.Data.Exhibition}");
             JoinRoomRequest?.Invoke(_exhibitionsSelector.SelectedItem.Data.Exhibition.ToString());
         }
         
         private void OnAvatarSelectorItemSelected(AvatarPreviewItem item)
         {
             _avatarSelector.ItemSelected -= OnAvatarSelectorItemSelected;
             _enterButton.interactable = true;
         }
     }
}