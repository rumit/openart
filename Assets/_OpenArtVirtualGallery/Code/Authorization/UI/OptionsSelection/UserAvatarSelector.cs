﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
    public class UserAvatarSelector : MonoBehaviour
    {
        public Action<AvatarPreviewItem> ItemSelected;


        [SerializeField] private Transform _contentContainer;
        [SerializeField] private AvatarPreviewItem _avatarPreviewItemRef;

        private readonly List<AvatarPreviewItem> _avatarPreviewItems = new ();
        private AppConfig.HeroConfigsList _heroAvatarsPictures;

        public AvatarPreviewItem SelectedItem { get; private set; }

        [Inject]
        private void Inject(AppConfig.HeroConfigsList heroAvatarsPictures)
        {
            _heroAvatarsPictures = heroAvatarsPictures;
        }

        private void OnDestroy()
        {
            ItemSelected -= OnItemSelected;
        }

        public void Init()
        {
            gameObject.SetActive(true);

            var heroAvatars = _heroAvatarsPictures.Get();
            for (var i = 0; i < heroAvatars.Count; i++)
            {
                var item = Instantiate(_avatarPreviewItemRef, _contentContainer);
                item.Init(this, heroAvatars[i].AvatarPicture, i);
                _avatarPreviewItems.Add(item);
            }

            ItemSelected += OnItemSelected;
        }

        private void OnItemSelected(AvatarPreviewItem item)
        {
            if(SelectedItem == item)
                return;
            
            if(SelectedItem != null)
                SelectedItem.Deselect();
            
            SelectedItem = item;
            SelectedItem.Select();
        }
    }
}