﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
    public class AvatarPreviewInteractor : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [Inject]private StylesConfig.BaseColors _baseColors;

        private AvatarPreviewItem _owner;
        private UserAvatarSelector _selector;
        private Image _frame;

        private Color _defFrameColor;
        private Color _selectionColor;

        private void Awake()
        {
            _frame = GetComponent<Image>();
        }

        public void Init(AvatarPreviewItem owner, UserAvatarSelector selector)
        {
            _owner = owner;
            _selector = selector;
            _defFrameColor = _baseColors.GetAvatarPreviewFrameColor();
            _selectionColor = _baseColors.GetAvatarPreviewSelectedFrameColor();
        }

        public void Select() => _frame.color = _selectionColor;
        
        public void Deselect() => _frame.color = _defFrameColor;

        public void OnPointerDown(PointerEventData eventData) { }

        public void OnPointerUp(PointerEventData eventData)
        {
            if(!eventData.pressPosition.Equals(eventData.position))
                return;
            
            _selector.ItemSelected?.Invoke(_owner);
        }
    }
}