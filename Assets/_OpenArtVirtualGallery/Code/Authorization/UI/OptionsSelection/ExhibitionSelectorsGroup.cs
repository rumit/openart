﻿using System;
using System.Linq;
using OpenArtVirtualGallery.UI.Items;
using UnityEngine;

namespace OpenArtVirtualGallery.Authorization.UI.OptionSelection
{
    public sealed class ExhibitionSelectorsGroup : ItemsGroup<ExhibitionSelectorsGroup.SelectorData>
    {
        protected override void Start()
        {
            base.Start();
            SelectItem(_items.First());
        }

        [Serializable]
        public sealed class SelectorData : IItemDataProvider
        {
            [SerializeField]private ScenesManager.Scenes _scene;
            public ScenesManager.Scenes Exhibition => _scene;
        }
    }
}