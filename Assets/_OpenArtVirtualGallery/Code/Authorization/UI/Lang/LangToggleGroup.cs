﻿using System.Linq;
using OpenArtVirtualGallery.UI.Items;
using OpenArtVirtualGallery.User;
using Zenject;

namespace OpenArtVirtualGallery.Authorization.UI.Lang
{
    public class LangToggleGroup : ItemsGroup<LangData>
    {
        private LangSelector _langSelector;
        private AppConfig.LangConfig _langConfig;
        private UserActions _userActions;

        [Inject]
        private void Inject(LangSelector langSelector, AppConfig.LangConfig langConfig, UserActions userActions)
        {
            _langSelector = langSelector;
            _langConfig = langConfig;
            _userActions = userActions;
        }
        
        protected override void Start()
        {
            base.Start();
            var item = _items.FirstOrDefault(i => i.Data.Lang == _langConfig.DefaultLang);
            SelectItem(item);
        }

        protected override void SelectItem(IItem<LangData> item)
        {
            base.SelectItem(item);
            _userActions.LangSelected?.Invoke(item.Data);
        }
    }
}