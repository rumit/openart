﻿using OpenArtVirtualGallery.UI.Items;
using OpenArtVirtualGallery.User;

namespace OpenArtVirtualGallery.Authorization.UI.Lang
{
    public class LangToggle : Item<LangData>
    {
        public override void Select()
        {
            _graphic.color = _itemsGroup.SelectedItemColor;
        }

        public override void Deselect()
        {
            _graphic.color = _defaultColor;
        }
    }
    
}