﻿using System;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static LangBaseValuesConfig;

namespace OpenArtVirtualGallery.Authorization.UI
{
    public class AuthorizationUI : MonoBehaviour
    {
        public event Action<string> ConnectToServerRequest;

        [Space(10)]
        [SerializeField] private GameObject _connectingDisplay;
        [SerializeField] private TextMeshProUGUI _titleField;
        [SerializeField] private TMP_InputField _nameField;
        [SerializeField] private TextMeshProUGUI _namePlaceholderField;
        [SerializeField] private Outline _nameFieldOutline;
        [SerializeField] private Button _enterBtn;

        private UserActions _userActions;
        private LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;
        private DebugLog _debugLog;
        private Color _nameFieldOutlineColor;
        private bool _repeatingPlayerID;
        private string _playFabPlayerIdCache;

        [Inject]
        private void Inject(UserActions userActions, LangValuesConfig<BaseLangIds>.LangValues langBaseValues, DebugLog debugLog)
        {
            _userActions = userActions;
            _langBaseValues = langBaseValues;
            _debugLog = debugLog;
        }

        private void Awake()
        {
            _connectingDisplay.SetActive(false);
            _nameFieldOutlineColor = _nameFieldOutline.effectColor;
            
            _nameField.onValueChanged.AddListener(OnNameFieldValueChanged);
            _enterBtn.onClick.AddListener(OnEnterButton);
        }
        
        private void OnDestroy()
        {
            _enterBtn.onClick.RemoveListener(OnEnterButton);
            _nameField.onValueChanged.RemoveListener(OnNameFieldValueChanged);
            _userActions.LangSelected -= OnLangSelected;
        }

        private void OnLangSelected(LangData data)
        {
            _titleField.text = _langBaseValues.Get(BaseLangIds.VirtualGalleryTitle);
            _enterBtn.GetComponentInChildren<TextMeshProUGUI>().text = _langBaseValues.Get(BaseLangIds.Enter);
            _namePlaceholderField.text = _langBaseValues.Get(BaseLangIds.EnterYouName);
        }

        private void Start()
        {
            _userActions.LangSelected += OnLangSelected;
        }

        private bool ValidateNameField(string value)
        {
            var isEmpty = string.IsNullOrWhiteSpace(value);
            _nameFieldOutline.effectColor = isEmpty ? Color.red : _nameFieldOutlineColor;
            return !isEmpty;
        }
        
        private void OnEnterButton()
        {
            if (!ValidateNameField(_nameField.text))
                return;

            _enterBtn.interactable = false;
            _connectingDisplay.SetActive(true);
            
            ConnectToServerRequest?.Invoke(_nameField.text);
        }
        
        private void OnNameFieldValueChanged(string value)
        {
            ValidateNameField(value);
        }
    }
}