﻿using System;

namespace Utils
{
	public static class EnumUtil
	{
		public static E GetEnumFromString<E>(string enumName) where E: Enum
		{
			return (E)Enum.Parse(typeof(E), enumName);
		}
		
		public static E GetEnumFromIndex<E>(int index) where E: Enum
		{
			return (E)Enum.GetValues(typeof(E)).GetValue(index);
		}

		public static int GetEnumLength<E>() where E: Enum
		{
			return Enum.GetValues(typeof(E)).Length;
		}
	}
}
