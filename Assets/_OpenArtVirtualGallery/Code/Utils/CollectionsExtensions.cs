﻿
using System;
using System.Collections.Generic;
using System.Linq;

public static class CollectionsExtensions
{
	private static readonly System.Random Random = new System.Random();
	
    public static List<T> GetRandomElements<T>(this IEnumerable<T> list, int elementsCount)
    {
        return list.OrderBy(arg => Guid.NewGuid()).Take(elementsCount).ToList();
    }

    public static T RandomChoice<T>(this IList<T> collection)
	{
		return RandomChoice(collection, Random);
	}

    public static T RandomChoice<T>(this IList<T> collection, System.Random random)
	{
		if (collection == null || collection.Count == 0)
		{
			return default(T);
		}
		return collection[random.Next(0, collection.Count)];
	}

	public static T RandomChoice<T>(this IEnumerable<T> collection, System.Random random)
	{
		if (collection == null)
		{
			return default(T);
		}
		var count = collection.Count();
		if (count == 0)
		{
			return default(T);
		}
		return collection.ElementAtOrDefault(random.Next(0, count));
	}

	public static T RandomChoice<T>(this T[,] array)
	{
		return RandomChoice(array, Random);
	}

	public static T RandomChoice<T>(this T[,] array, System.Random random)
	{
		return array[random.Next(0, array.GetLength(0)), random.Next(0, array.GetLength(1))];
	}

	public static T[] GetRandomArray<T>(this IList<T> list, int count)
	{
		return GetRandomArray(list, count, Random);
	}

	public static T[] GetRandomArray<T>(this IList<T> list, int count, System.Random random)
	{
		var l = GetRandomList(list, count, random);
		return l.ToArray();
	}

	public static List<T> GetRandomList<T>(this IList<T> list, int count, System.Random random)
	{
		count = Math.Min(count, list.Count);
		var l = new List<T>(list);
		l.Shuffle(random);
		return l.Slice(0, count);
	}

	public static bool AddIfNotNull<T>(this IList<T> collection, T value) where T : class
	{
		if (value == null)
			return false;
		collection.Add(value);
		return true;
	}

	public static T[] SliceToArray<T>(this IList<T> list, int from, int count)
	{
		if (list.Count < from)
		{
			count = 0;
		}
		count = Math.Min(list.Count - from, count);
		T[] ret = new T[count];
		for (int i = 0; i < count; ++i)
		{
			ret[i] = list[from + i];
		}
		return ret;
	}
	
	public static void Shuffle<T>(this IList<T> list)
	{
		Shuffle(list, Random);
	}

	public static void Shuffle<T>(this IList<T> list, System.Random random)
	{
		int n = list.Count;
		while (n > 1)
		{
			int k = (random.Next(0, n));
			n--;
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static void InsertionSort<T>(this IList<T> list, System.Comparison<T> comparison)
	{
		if (list == null)
			throw new System.ArgumentNullException("list");
		if (comparison == null)
			throw new System.ArgumentNullException("comparison");

		int count = list.Count;
		for (int j = 1; j < count; j++)
		{
			T key = list[j];

			int i = j - 1;
			for (; i >= 0 && comparison(list[i], key) > 0; i--)
			{
				list[i + 1] = list[i];
			}
			list[i + 1] = key;
		}
	}

	public static List<T> Slice<T>(this IList<T> list, int from, int count)
	{
		if (list.Count < from)
		{
			count = 0;
		}
		count = Math.Min(list.Count - from, count);
		var ret = new List<T>();
		for (int i = 0; i < count; ++i)
		{
			ret.Add(list[from + i]);
		}
		return ret;
	}

	public static int FindIndex<T>(this IList<T> array, System.Predicate<T> match)
	{
		int index = 0;
		foreach (T v in array)
		{
			if (match(v))
			{
				return index;
			}
			index++;
		}
		return -1;
	}

	public static V TryGetOrCreate<K, V>(this Dictionary<K, V> dict, K key)
		where V : new()
	{
		V val;
		if (!dict.TryGetValue(key, out val))
		{
			val = new V();
			dict.Add(key, val);
		}
		return val;
	}

	public static V GetOrDefault<K, V>(this Dictionary<K, V> dict, K key)
	{
		V val;
		if (!dict.TryGetValue(key, out val))
		{
			return default(V);
		}
		return val;
	}

	public static V GetOrDefault<K, V>(this Dictionary<K, V> dict, K key, V defaultValue)
	{
		V val;
		if (!dict.TryGetValue(key, out val))
		{
			return defaultValue;
		}
		return val;
	}

	public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
	{
		if (collection == null)
		{
			throw new ArgumentNullException("Collection is null");
		}

		foreach (var item in collection)
		{
			source.Add(item.Key, item.Value);
		}
	}

	public class ReverseComparer<T> : Comparer<T>
	{
		private readonly IComparer<T> inner;
		public ReverseComparer() : this(null) { }
		public ReverseComparer(IComparer<T> inner)
		{
			this.inner = inner ?? Comparer<T>.Default;
		}
		public override int Compare(T x, T y) { return inner.Compare(y, x); }

		private static ReverseComparer<T> _defaultComparer;
		public new static ReverseComparer<T> Default
		{
			get { return _defaultComparer ?? (_defaultComparer = new ReverseComparer<T>()); }
		}
	}
	
	public static string ToDebugString<TKey, TValue> (this IDictionary<TKey, TValue> dictionary)
	{
		return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
	}	
    
}
