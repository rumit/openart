﻿using UnityEngine;

namespace Utils
{
	public static class GameObjectHelper
	{
		/// <summary>
		/// рекурсивно устанавливает заданный layer для всех детей заданного экземпляра Transform
		/// </summary>
		/// <param name="layer">присваиваемый layer</param>
		/// <param name="ignoreLayer">layer, который нужно проигнорировать</param>
		public static void SetLayerRecursively(this GameObject parent, int layer, int ignoreLayer = -1)
		{
			parent.layer = layer;
			var childs = parent.transform.GetComponentsInChildren<Transform>(true);
			for (int i = 0, len = childs.Length; i < len; i++)
			{
				var go = childs[i].gameObject;
				if (ignoreLayer > -1)
				{
					if (go.layer != ignoreLayer) go.layer = layer;
				}
				else
				{
					go.layer = layer;
				}
			}
		}
	}
}
