﻿using System;
using UnityEngine;

namespace OpenArtVirtualGallery.Utils
{
    public static class Logger
    {
        public static void Log(object message, Color? color = null)
        {
            var t = $"{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}";
            var m = $"<color=#{ColorUtility.ToHtmlStringRGB(color ?? Color.yellow)}>{t} {message}</color>";
            Debug.Log(m);
        }
    
        public static void LogError(object message)
        {
            var m = $"<color=#{ColorUtility.ToHtmlStringRGB(Color.red)}>{message}</color>";
            Debug.Log(m);
        }
    }
}

