﻿using UnityEngine;

namespace Utils
{
    public static class TransformExtensions
    {
        public static Transform[] GetAllChildrens(this Transform transform)
        {
            var childs = new Transform[transform.childCount];
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                childs[i] = child;
            }
            return childs;
        }
    }
}