﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

[AttributeUsage(AttributeTargets.All)]
[Conditional("UNITY_EDITOR")]
public class DropdownNameAttribute : Attribute
{
    public string Name;

    public DropdownNameAttribute(string name)
    {
        Name = name;
    }
}

[AttributeUsage(AttributeTargets.All)]
[Conditional("UNITY_EDITOR")]
public class CustomAssetSelectorAttribute : Attribute
{
    /// <summary>True by default.</summary>
    public bool IsUniqueList = true;

    /// <summary>
    /// True by default. If the ValueDropdown attribute is applied to a list, then disabling this,
    /// will render all child elements normally without using the ValueDropdown. The ValueDropdown will
    /// still show up when you click the add button on the list drawer, unless <see cref="F:Sirenix.OdinInspector.AssetSelectorAttribute.DisableListAddButtonBehaviour" /> is true.
    /// </summary>
    public bool DrawDropdownForListElements = true;

    /// <summary>False by default.</summary>
    public bool DisableListAddButtonBehaviour;

    /// <summary>
    /// If the ValueDropdown attribute is applied to a list, and <see cref="F:Sirenix.OdinInspector.AssetSelectorAttribute.IsUniqueList" /> is set to true, then enabling this,
    /// will exclude existing values, instead of rendering a checkbox indicating whether the item is already included or not.
    /// </summary>
    public bool ExcludeExistingValuesInList;

    /// <summary>
    /// If the dropdown renders a tree-view, then setting this to true will ensure everything is expanded by default.
    /// </summary>
    public bool ExpandAllMenuItems = true;

    /// <summary>By default, the dropdown will create a tree view.</summary>
    public bool FlattenTreeView;

    /// <summary>
    /// Gets or sets the width of the dropdown. Default is zero.
    /// </summary>
    public int DropdownWidth;

    /// <summary>
    /// Gets or sets the height of the dropdown. Default is zero.
    /// </summary>
    public int DropdownHeight;

    /// <summary>
    /// Gets or sets the title for the dropdown. Null by default.
    /// </summary>
    public string DropdownTitle;

    /// <summary>
    /// Specify which folders to search in. Specifying no folders will make it search in your entire project.
    /// Use the <see cref="P:Sirenix.OdinInspector.AssetSelectorAttribute.Paths" /> property for a more clean way of populating this array through attributes.
    /// </summary>
    public string[] SearchInFolders;

    /// <summary>
    /// The filters we should use when calling AssetDatabase.FindAssets.
    /// </summary>
    public string Filter;

    public bool ShortNames;

    /// <summary>
    /// <para>
    /// Specify which folders to search in. Specifying no folders will make it search in your entire project.
    /// You can decalir multiple paths using '|' as the seperator.
    /// Example: <code>[AssetList(Paths = "Assets/Textures|Assets/Other/Textures")]</code>
    /// </para>
    /// <para>
    /// This property is simply a more clean way of populating the <see cref="F:Sirenix.OdinInspector.AssetSelectorAttribute.SearchInFolders" /> array.
    /// </para>
    /// </summary>
    public string Paths
    {
        set => this.SearchInFolders = ((IEnumerable<string>) value.Split('|')).Select<string, string>((Func<string, string>) (x => x.Trim().Trim('/', '\\')))
            .ToArray<string>();
        get => this.SearchInFolders != null ? string.Join(",", this.SearchInFolders) : (string) null;
    }
}

[AttributeUsage(AttributeTargets.All)]
[Conditional("UNITY_EDITOR")]
public class TypesDropdownAttribute : Attribute
{
    /// <summary>
    /// The number of items before enabling search. Default is 10.
    /// </summary>
    public int NumberOfItemsBeforeEnablingSearch;

    /// <summary>False by default.</summary>
    public bool IsUniqueList;

    /// <summary>
    /// True by default. If the ValueDropdown attribute is applied to a list, then disabling this,
    /// will render all child elements normally without using the ValueDropdown. The ValueDropdown will
    /// still show up when you click the add button on the list drawer, unless <see cref="F:Sirenix.OdinInspector.ValueDropdownAttribute.DisableListAddButtonBehaviour" /> is true.
    /// </summary>
    public bool DrawDropdownForListElements;

    /// <summary>False by default.</summary>
    public bool DisableListAddButtonBehaviour;

    /// <summary>
    /// If the ValueDropdown attribute is applied to a list, and <see cref="F:Sirenix.OdinInspector.ValueDropdownAttribute.IsUniqueList" /> is set to true, then enabling this,
    /// will exclude existing values, instead of rendering a checkbox indicating whether the item is already included or not.
    /// </summary>
    public bool ExcludeExistingValuesInList;

    /// <summary>
    /// If the dropdown renders a tree-view, then setting this to true will ensure everything is expanded by default.
    /// </summary>
    public bool ExpandAllMenuItems;

    /// <summary>
    /// If true, instead of replacing the drawer with a wide dropdown-field, the dropdown button will be a little button, drawn next to the other drawer.
    /// </summary>
    public bool AppendNextDrawer;

    /// <summary>
    /// Disables the the GUI for the appended drawer. False by default.
    /// </summary>
    public bool DisableGUIInAppendedDrawer;

    /// <summary>
    /// By default, a single click selects and confirms the selection.
    /// </summary>
    public bool DoubleClickToConfirm;

    /// <summary>By default, the dropdown will create a tree view.</summary>
    public bool FlattenTreeView;

    /// <summary>
    /// Gets or sets the width of the dropdown. Default is zero.
    /// </summary>
    public int DropdownWidth;

    /// <summary>
    /// Gets or sets the height of the dropdown. Default is zero.
    /// </summary>
    public int DropdownHeight;

    /// <summary>
    /// Gets or sets the title for the dropdown. Null by default.
    /// </summary>
    public string DropdownTitle;

    /// <summary>False by default.</summary>
    public bool SortDropdownItems;

    /// <summary>Whether to draw all child properties in a foldout.</summary>
    public bool HideChildProperties;

    public Type TypeOverride;
    public bool EnablePresets = false;
    public string PresetsPath;

    /// <summary>Creates a dropdown menu for a property.</summary>
    public TypesDropdownAttribute(Type typeOverride = null)
    {
        NumberOfItemsBeforeEnablingSearch = 10;
        DrawDropdownForListElements = true;
        TypeOverride = typeOverride;
    }
}


// Надо избавиться от конфликтов (нужен namespace)
public static class EditorUtilities
{
#if UNITY_EDITOR
    public static string GetTypeDropdownName(Type type, bool nicifyName = true)
    {
        return ((DropdownNameAttribute) Attribute.GetCustomAttribute(type, typeof(DropdownNameAttribute)))?.Name ??
               (nicifyName ? ObjectNames.NicifyVariableName(type.Name) : type.ToString());
    }
    
    public static bool IsEnumerableType(Type type)
    {
        return type.GetInterface(nameof(IEnumerable)) != null;
    }

    // includeNull пока на будущее (null не установить в ValueDropdown)
    public static IEnumerable AllDerivedTypesDropdownItems(Type type, bool includePresets = false, string presetPath = null, bool includeNull = false)
    {
        var typesFolder = includePresets ? "Types/" : "";
        var serializedObjectType = typeof(ScriptableObject);

        var types = type.Assembly.GetTypes()
            .Where(x => !x.IsAbstract) // Excludes BaseClass
            .Where(x => !x.IsGenericTypeDefinition) // Excludes generics
            .Where(type.IsAssignableFrom) // Excludes classes not inheriting from BaseClass
            .Where(x => !serializedObjectType.IsAssignableFrom(x))
            .Select(x => new ValueDropdownItem(
                GetTypeDropdownName(x),
                Activator.CreateInstance(x))
            );

        var presetTypes = type.Assembly.GetTypes()
            .Where(x => !x.IsAbstract) // Excludes BaseClass
            .Where(x => !x.IsGenericTypeDefinition) // Excludes generics
            .Where(type.IsAssignableFrom) // Excludes classes not inheriting from BaseClass
            .Where(x => serializedObjectType.IsAssignableFrom(x));


        if (includePresets)
        {
            if (type.IsSubclassOf(typeof(Component)))
            {
                types = types.Concat(EditorUtilities.GetAssetsAtPath(type, presetPath, "*.prefab")
                    .Select(x => new ValueDropdownItem(AssetDatabase.GetAssetPath(x).Replace("Assets/" + presetPath + "/", ""), x)));
            }
            else
            {

                types = presetTypes.Select(presetType => AssetDatabase.FindAssets($"t:{presetType.Name}")
                        .Select(AssetDatabase.GUIDToAssetPath)
                        .Select(x => new ValueDropdownItem("Presets/" + Path.GetFileNameWithoutExtension(x), AssetDatabase.LoadAssetAtPath<ScriptableObject>(x))))
                    .Aggregate(types, (current, presets) => current.Concat(presets));

            }
        }

        if (includeNull)
        {
            types = types.Prepend(new ValueDropdownItem("NONE", null));
        }

        return types;
    }

    public static IEnumerable GetAllScriptableObjects(string type = "ScriptableObject")
    {
        return AssetDatabase.FindAssets($"t:{type}")
            .Select(AssetDatabase.GUIDToAssetPath)
            .Select(x => new ValueDropdownItem(Path.GetFileNameWithoutExtension(x), AssetDatabase.LoadAssetAtPath<ScriptableObject>(x)));
    }
    
    public static List<T> GetAssetsAtPath<T>(string folderPath, string filter = null) where T : UnityEngine.Object
    {
        var result = new List<T>();
        if (filter == null) { filter = "*.*"; }
        var files = Directory.EnumerateFiles($"{Application.dataPath}/{folderPath}", filter, SearchOption.AllDirectories);
        foreach (var fileName in files)
        {
            var path = "Assets/" + fileName.Replace(Application.dataPath, "");
            var asset = AssetDatabase.LoadAssetAtPath<T>($"{path}");
            if (asset == null)
                continue;
            result.Add(asset);
        }
        return result;
    }

    public static List<Object> GetAssetsAtPath(Type type, string folderPath, string filter = null)
    {
        var result = new List<Object>();
        if (filter == null) { filter = "*.*"; }
        var files = Directory.EnumerateFiles($"{Application.dataPath}/{folderPath}", filter, SearchOption.AllDirectories);
        foreach (var fileName in files)
        {
            var path = "Assets/" + fileName.Replace(Application.dataPath, "");
            var asset = AssetDatabase.LoadAssetAtPath($"{path}", type);
            if (asset == null)
                continue;
            result.Add(asset);
        }
        return result;
    }

#endif
}
