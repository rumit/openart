﻿using UnityEngine;

namespace Utils
{
	public static class ColorHelper
	{
		//Note that Color32 and Color implictly convert to each other.You may pass a Color object to this method without first casting it.
		public static string ColorToHex(this Color color)
		{
			string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
			return hex;
		}
	}
}

