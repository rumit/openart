﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Gallery.Exhibitors;

namespace OpenArtVirtualGallery.Gallery
{
    public class GalleryActions
    {
        public Action<ExhibitImage, ExhibitImage[]> ShowPicture;
        public Action<ExhibitorConfig> ShowExtendedInfo;
        public Action<ExhibitorConfig[]> ShowVideoArtWorks;
    }
}