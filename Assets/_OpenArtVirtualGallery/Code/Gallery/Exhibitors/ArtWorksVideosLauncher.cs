﻿using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using UnityEngine.Video;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ArtWorksVideosLauncher : MonoBehaviour
    {
        [SerializeField]private ExhibitionsSceneConfig.GalleryFloors _galleryLevel;
        [SerializeField]private VideoPlayer _videoPlayer;
        [SerializeField] private ExhibitorConfig[] _configs;

        private GalleryActions _galleryActions;
        private AppConfig.URLs _urls;
        private UserTeleportsManager _userTeleportsManager;
        private Vector2 _pressPos;

        [Inject]
        private void Inject(GalleryActions galleryActions, UserTeleportsManager userTeleportsManager, AppConfig.URLs urls)
        {
            _galleryActions = galleryActions;
            _userTeleportsManager = userTeleportsManager;
            _urls = urls;
        }

        private void Start()
        {
            if (_videoPlayer != null)
                _videoPlayer.url = _urls.VideosUrl + "video_blackscreen.mp4";
        }

        private void OnMouseDown()
        {
            _pressPos = Input.mousePosition;
        }

        private void OnMouseUpAsButton()
        {
            if(_galleryLevel == ExhibitionsSceneConfig.GalleryFloors.Second && !_userTeleportsManager.IsPlayerOnSecondLevel ||
               _galleryLevel == ExhibitionsSceneConfig.GalleryFloors.First && _userTeleportsManager.IsPlayerOnSecondLevel)
                return;
            
            var dist = Mathf.Abs(_pressPos.sqrMagnitude - Input.mousePosition.sqrMagnitude);
            if (dist < 6000)
            {
                _galleryActions.ShowVideoArtWorks.Invoke(_configs);
            }
        }
    }
}