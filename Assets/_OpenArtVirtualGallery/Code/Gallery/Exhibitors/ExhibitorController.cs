﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using OpenArtVirtualGallery.Visitor;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ExhibitorController : MonoBehaviour
    {
        public Action<ExhibitImage> ExhibitImageSelected;

        [SerializeField]private ExhibitionsSceneConfig.GalleryFloors _galleryLevel;
        [SerializeField] private ExhibitorConfig _config;
        [SerializeField] private ExhibitImage[] _pictures;
        [SerializeField] private Button _infoPlatte;

        private GalleryActions _galleryActions;
        private Camera _camera;
        private LangSelector _langSelector;
        private UserTeleportsManager _userTeleportsManager;

        [Inject]
        private void Inject(GalleryActions galleryActions, LangSelector langSelector, UserTeleportsManager userTeleportsManager)
        {
            _galleryActions = galleryActions;
            _langSelector = langSelector;
            _userTeleportsManager = userTeleportsManager;
        }

        private void OnDestroy()
        {
            ExhibitImageSelected -= OnExhibitImageSelected;
            _infoPlatte.onClick.RemoveListener(OnInfoPlatteClick);
        }

        private void Start()
        {
            _camera = Camera.main;
            ExhibitImageSelected += OnExhibitImageSelected;
            _infoPlatte.onClick.AddListener(OnInfoPlatteClick);
            
            _infoPlatte.GetComponentInChildren<TextMeshProUGUI>().text = _langSelector.GetLocalText(_config.Name);
        }

        private void OnInfoPlatteClick()
        {
            if(CheckCondition(_infoPlatte.transform.position))
                _galleryActions.ShowExtendedInfo?.Invoke(_config);
        }

        private void OnExhibitImageSelected(ExhibitImage image)
        {
            if(CheckCondition(image.transform.position))
                _galleryActions.ShowPicture?.Invoke(image, _pictures);
        }

        private bool CheckCondition(Vector3 clickPos)
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit, Vector3.Distance(_camera.transform.position, clickPos) * .9f))
                return false;

            if(_galleryLevel == ExhibitionsSceneConfig.GalleryFloors.Second && !_userTeleportsManager.IsPlayerOnSecondLevel ||
               _galleryLevel == ExhibitionsSceneConfig.GalleryFloors.First && _userTeleportsManager.IsPlayerOnSecondLevel)
                return false;
            return true;
        }
    }
}