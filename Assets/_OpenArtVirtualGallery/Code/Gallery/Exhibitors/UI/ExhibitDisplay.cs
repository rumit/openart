﻿using System;
using DG.Tweening;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    [RequireComponent(typeof(PicturesViewer))]
    public class ExhibitDisplay : MonoBehaviour
    {
        [SerializeField]private Button _closeBtn;

        private UserActions _userActions;
        private GalleryActions _galleryActions;
        private VisitorsManager _visitorsManager;
        private Transform _container;
        private IExtendedExhibitDisplay _currentOpenedDisplay;

        [Inject]
        private void Inject(GalleryActions galleryActions, UserActions userActions, VisitorsManager visitorsManager)
        {
            _galleryActions = galleryActions;
            _userActions = userActions;
            _visitorsManager = visitorsManager;
        }
        
        private void OnDestroy()
        {
            _closeBtn.onClick.RemoveListener(OnCloseDisplay);
            
            _galleryActions.ShowPicture -= OnShowPictures;
            _galleryActions.ShowExtendedInfo -= OnShowExtendedInfo;
            _galleryActions.ShowVideoArtWorks -= OnShowVideoArtWorks;

        }

        private void Awake()
        {
            _container = transform.GetChild(0);
            _container.transform.localScale = Vector3.zero;
            _container.gameObject.SetActive(false);

            _galleryActions.ShowPicture += OnShowPictures;
            _galleryActions.ShowExtendedInfo += OnShowExtendedInfo;
            _galleryActions.ShowVideoArtWorks += OnShowVideoArtWorks;

            _closeBtn.onClick.AddListener(OnCloseDisplay);
        }

#if UNITY_WEBGL
        private bool CheckConditions()
        {
            var playerHeroModel = _visitorsManager.GetLocalPlayerHero().Model;
            var input = playerHeroModel.PlayerMotionController.Input;
            return input.Move.Equals(Vector2.zero) && input.Look.Equals(Vector2.zero);
        }
#endif

        private void OnShowPictures(ExhibitImage image, ExhibitImage[] pictures)
        {
            Open(() =>
            {
                var picturesViewer = GetComponent<PicturesViewer>();
                picturesViewer.Show(image, pictures);
                _currentOpenedDisplay = picturesViewer;
            });
        }
        
        private void OnShowExtendedInfo(ExhibitorConfig config)
        {
            Open(() =>
            {
                var infoDisplay = GetComponent<ExtendedInfoViewer>();
                infoDisplay.Show(config);
                _currentOpenedDisplay = infoDisplay;
            });
        }

        private void OnShowVideoArtWorks(ExhibitorConfig[] configs)
        {
            if(_currentOpenedDisplay != null)
                return;
            Open(() =>
            {
                var artworkVideosViewer = GetComponent<ArtworkVideoViewer>();
                artworkVideosViewer.Show(configs);
                _currentOpenedDisplay = artworkVideosViewer;
            });
        }

        public void ShowModule<T>() where T : IExtendedExhibitDisplay
        {
            Open(() =>
            {
                var module = GetComponent<T>();
                module.Show();
                _currentOpenedDisplay = module;
            });
        }

        private void Open(Action callback)
        {
#if UNITY_WEBGL
            if(!CheckConditions())
                return;
#endif
            
            
            _userActions.TogglePlayerNavigation?.Invoke(false);
            _container.gameObject.SetActive(true);
            _container.transform.DOScale(1, .2f).SetEase(Ease.OutSine)
                .OnComplete(() => callback?.Invoke());
        }
        
        private void OnCloseDisplay()
        {
            _userActions.TogglePlayerNavigation?.Invoke(true);
            _container.transform.DOScale(0, .2f).SetEase(Ease.InSine)
                .OnComplete(() =>
                {
                    _currentOpenedDisplay?.Hide();
                    _currentOpenedDisplay = null;
                    _container.gameObject.SetActive(false);
                });
        }
    }
}