﻿using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ViewerContainer : MonoBehaviour
    {
        public void SetActive(bool value) => gameObject.SetActive(value);
    }
}