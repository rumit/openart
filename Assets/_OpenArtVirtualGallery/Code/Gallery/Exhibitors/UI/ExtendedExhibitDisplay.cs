﻿using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public abstract class ExtendedExhibitDisplay : MonoBehaviour, IExtendedExhibitDisplay
    {
        public abstract void Hide();
        public virtual void Show() { }
    }
}