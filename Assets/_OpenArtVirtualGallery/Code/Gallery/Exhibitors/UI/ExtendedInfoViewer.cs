﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ExtendedInfoViewer : ExtendedExhibitDisplay, IExtendedExhibitDisplay
    {
        [Inject] private MediaEvents _mediaEvents;
        [Inject] private LangSelector _langSelector;
        
        [SerializeField] private ViewerContainer _container;
        [SerializeField] private TextMeshProUGUI _infoField;
        
        [Space(10)]
        [SerializeField] private AudioControl _audioControlRef;

        [SerializeField] private Transform _audioControlsContainer;
        [SerializeField] private VideoControl _videoControlRef;
        [SerializeField] private Transform _videoControlsContainer;
        
        private ExhibitorConfig _config;
        private VideoViewer _videoViewer;
        
        private readonly List<AudioControl> _audioControls = new List<AudioControl>();
        private readonly List<VideoControl> _videoControls = new List<VideoControl>();

        private void Awake()
        {
            _videoViewer = _container.GetComponent<VideoViewer>();
            Hide();
        }

        public void Show(ExhibitorConfig config)
        {
            _container.SetActive(true);
            _config = config;

            SetAboutInfo();
            InitMediaControls();
        }
        
        private void SetAboutInfo()
        {
            var info = _langSelector.GetLocalText(_config.About);
            
            var links = _config.Links;
            if (links != null || links.Length > 0)
            {
                var linksStr = "\n\n";
                foreach (var linkData in links)
                {
                    var commentBefore = string.IsNullOrEmpty(linkData.CommentBefore) ? "" : $"{linkData.CommentBefore} "; 
                    linksStr += $"{commentBefore}<link=\"{linkData.URL}\"><u>{linkData.URL}</u></link>\n";
                }
                info += linksStr;
            }
                
            _infoField.text = info;
        }

        private void InitMediaControls()
        {
            var media = _config.Media;
            if (media.Audio != null && media.Audio.Length > 0)
            {
                _mediaEvents.AudioPlayedCallback += OnAudioPlayed;
                foreach (var clip in media.Audio)
                {
                    var audioControl = Instantiate(_audioControlRef, _audioControlsContainer);
                    audioControl.Init(clip, _mediaEvents.AudioPlayedCallback);
                    _audioControls.Add(audioControl);
                }
            }

            if (media.Videos != null && media.Videos.Length > 0)
            {
                _mediaEvents.VideoPlayedCallback += OnVideoPlayed;
                _mediaEvents.VideoStoppedCallback += OnVideoStopped;
                foreach (var url in media.Videos)
                {
                    var videoControl = Instantiate(_videoControlRef, _videoControlsContainer);
                    videoControl.Init(url, _videoViewer, _mediaEvents.VideoPlayedCallback, _mediaEvents.VideoStoppedCallback);
                    _videoControls.Add(videoControl);
                }
            }
        }

        private void OnAudioPlayed(MediaControl mediaControl)
        {
            CheckWhetherSomeMediaPlaying(mediaControl);
        }
        
        private void OnVideoPlayed(MediaControl mediaControl)
        {
            _infoField.gameObject.SetActive(false);
            CheckWhetherSomeMediaPlaying(mediaControl);
        }

        private void CheckWhetherSomeMediaPlaying(MediaControl mediaControl)
        {
            var audioControl = _audioControls.FirstOrDefault(a => a.isPlaying && a != mediaControl);
            if(audioControl != null)
                audioControl.Stop();

            var videoControl = _videoControls.FirstOrDefault(v => v.isPlaying && v != mediaControl);
            if(videoControl != null)
                videoControl.Stop();
        }
        
        private void OnVideoStopped()
        {
            if(_infoField.gameObject == null)
                return;
            _infoField.gameObject.SetActive(true);
        }

        public override void Hide()
        {
            _container.SetActive(false);
            
            _mediaEvents.AudioPlayedCallback -= OnAudioPlayed;
            _mediaEvents.VideoPlayedCallback -= OnVideoPlayed;
            
            foreach (var audioControl in _audioControls)
                Destroy(audioControl.gameObject);
            foreach (var videoControl in _videoControls)
                Destroy(videoControl.gameObject);
            
            _audioControls.Clear();
            _videoControls.Clear();
        }
    }
}