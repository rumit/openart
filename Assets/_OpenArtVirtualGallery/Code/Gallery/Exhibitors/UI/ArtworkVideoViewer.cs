﻿using System.Collections.Generic;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ArtworkVideoViewer : ExtendedExhibitDisplay, IExtendedExhibitDisplay
    {
        [Inject] private MediaEvents _mediaEvents;
        [Inject] private LangSelector _langSelector;
        
        [SerializeField]private ViewerContainer _container;
        [SerializeField]private ExtendedVideoControl _videoControlRef;
        [SerializeField]private Transform _videoControlsContainer;
        [SerializeField]private Button _leftArrowBtn;
        [SerializeField]private Button _rightArrowBtn;

        private readonly List<MediaControl> _videoControls = new ();
        private VideoViewer _player;
        private int _currentVideoIndex;
        private MediaControl _currentActiveVideoControl;

        private void Awake()
        {
            _player = _container.GetComponent<VideoViewer>();
            _container.SetActive(false);
            
            _leftArrowBtn.onClick.AddListener(OnLeftArrowClick);
            _rightArrowBtn.onClick.AddListener(OnRightArrowClick);
        }

        private void OnDestroy()
        {
            _mediaEvents.VideoPlayedCallback -= OnVideoPlayed;
            _leftArrowBtn.onClick.RemoveListener(OnLeftArrowClick);
            _rightArrowBtn.onClick.RemoveListener(OnRightArrowClick);
        }

        public void Show(ExhibitorConfig[] configs)
        {
            _mediaEvents.VideoPlayedCallback += OnVideoPlayed;
            _container.SetActive(true);
            
            for (var i = 0; i < configs.Length; i++)
            {
                var config = configs[i];
                var videoData = config.Media.ArtVideos;
                foreach (var data in videoData)
                {
                    var videoControl = Instantiate(_videoControlRef, _videoControlsContainer);
                    var projectName = $"{_langSelector.GetLocalText(data.Info)}";
                    var authorName = $"{_langSelector.GetLocalText(config.Name)}";
                    videoControl.Init(data, projectName, authorName, _player, _mediaEvents.VideoPlayedCallback, _mediaEvents.VideoStoppedCallback);
                    _videoControls.Add(videoControl);
                }
            }
            
            PlayVideo();
        }
        
        public override void Hide()
        {
            _currentActiveVideoControl = null;
            _mediaEvents.VideoPlayedCallback -= OnVideoPlayed;
            foreach (var control in _videoControls)
                Destroy(control.gameObject);
            _videoControls.Clear();
            _container.SetActive(false);
            _currentVideoIndex = 0;
        }

        private void PlayVideo()
        {
            _videoControls[_currentVideoIndex].Play();
        }

        private void OnVideoPlayed(MediaControl mediaControl)
        {
            if(_currentActiveVideoControl != null && _currentActiveVideoControl != mediaControl)
                _currentActiveVideoControl.Stop();
            
            _currentVideoIndex = _videoControls.IndexOf(mediaControl);
            CheckArrowButtonsInteractivity();
            
            _currentActiveVideoControl = mediaControl;
        }

        private void CheckArrowButtonsInteractivity()
        {
            _rightArrowBtn.interactable = _currentVideoIndex < _videoControls.Count - 1;
            _leftArrowBtn.interactable = _currentVideoIndex > 0;
        }
        
        private void OnRightArrowClick()
        {
            if(_currentVideoIndex < _videoControls.Count - 1)
                _currentVideoIndex++;
            PlayVideo();
        }

        private void OnLeftArrowClick()
        {
            if(_currentVideoIndex > 0)
                _currentVideoIndex--;
            PlayVideo();
        }
    }
}