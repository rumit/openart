﻿using System;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class MediaEvents
    {
        public Action<MediaControl> AudioPlayedCallback;
        public Action<MediaControl> VideoPlayedCallback;
        public Action VideoStoppedCallback;
    }
}