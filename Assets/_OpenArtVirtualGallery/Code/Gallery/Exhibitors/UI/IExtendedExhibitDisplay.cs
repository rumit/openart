﻿namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public interface IExtendedExhibitDisplay
    {
        void Show();
        void Hide();
    }
    
}