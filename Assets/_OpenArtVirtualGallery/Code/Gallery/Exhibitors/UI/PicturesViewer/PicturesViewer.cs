﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using ModestTree;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class PicturesViewer : ExtendedExhibitDisplay, IExtendedExhibitDisplay
    {
        [SerializeField] private ViewerContainer _container;
        [SerializeField] private ScrollRect _descriptionScrollRect;
        [SerializeField] private Button _leftArrowBtn;
        [SerializeField] private Button _rightArrowBtn;
        [SerializeField] private Image _pictureHolder;
        [SerializeField] private TextMeshProUGUI _descriptionField;
        [SerializeField] private TextMeshProUGUI _extendedDescriptionField;

        private LangSelector _langSelector;
        private int _currentPictureIndex;
        private ExhibitImage[] _currentPictureSet;
        private TweenerCore<Color, Color, ColorOptions> _fadeTween;
        private GameObject _extendedDescriptionContainer;
        private RectTransform _extendedDescriptionRectTransform;

        [Inject]
        private void Inject(LangSelector langSelector)
        {
            _langSelector = langSelector;
        }

        private void Awake()
        {
            _container.SetActive(false);

            _extendedDescriptionContainer = _extendedDescriptionField.transform.parent.gameObject;
            _extendedDescriptionContainer.gameObject.SetActive(false);
            _extendedDescriptionRectTransform = _extendedDescriptionField.GetComponent<RectTransform>();

            _leftArrowBtn.onClick.AddListener(OnLeftArrowClick);
            _rightArrowBtn.onClick.AddListener(OnRightArrowClick);
        }
        
        private void OnDestroy()
        {
            _leftArrowBtn.onClick.RemoveListener(OnLeftArrowClick);
            _rightArrowBtn.onClick.RemoveListener(OnRightArrowClick);
        }

        public void Show(ExhibitImage image, ExhibitImage[] pictures)
        {
            _container.SetActive(true);
            _currentPictureIndex = pictures.IndexOf(image);
            _currentPictureSet = pictures;
            ShowPicture();
            _pictureHolder.color = Color.white;
        }

        public override void Hide()
        {
            _pictureHolder.sprite = null;
            _pictureHolder.color = Color.clear;
            _descriptionField.text = "";
            _container.SetActive(false);
        }
        
        private void ShowPicture(bool withFade = false)
        {
            _rightArrowBtn.interactable = _currentPictureIndex < _currentPictureSet.Length - 1;
            _leftArrowBtn.interactable = _currentPictureIndex > 0;

            var current = _currentPictureSet[_currentPictureIndex];
            ShowDescriptions(current);

            if (!withFade)
            {
                _pictureHolder.sprite = current.Image.sprite;
                return;
            }
            
            _fadeTween?.Kill();
            _fadeTween = _pictureHolder.DOFade(0, .1f)
                .OnComplete(() =>
                {
                    _pictureHolder.sprite = current.Image.sprite;
                    _pictureHolder.DOFade(1, .1f);
                });
        }

        private void ShowDescriptions(ExhibitImage exhibitImage)
        {
            _descriptionField.text = _langSelector.GetLocalText(exhibitImage.Description);
            var extendedDescription = _langSelector.GetLocalText(exhibitImage.ExtendedDescription);
            if (!string.IsNullOrEmpty(extendedDescription))
            {
                _extendedDescriptionContainer.gameObject.SetActive(true);
                _extendedDescriptionField.text = "";
                _extendedDescriptionRectTransform.anchoredPosition = new Vector2(_extendedDescriptionRectTransform.anchoredPosition.x, 0);
                _extendedDescriptionField.text = extendedDescription;
            }
            else
                _extendedDescriptionContainer.gameObject.SetActive(false);
        }

        private void OnRightArrowClick()
        {
            if(_currentPictureIndex < _currentPictureSet.Length - 1)
                _currentPictureIndex++;
            ShowPicture(true);
        }

        private void OnLeftArrowClick()
        {
            if(_currentPictureIndex > 0)
                _currentPictureIndex--;
            
            ShowPicture(true);
        }
    }
}