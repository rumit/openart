﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ExhibitImage : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private ExhibitorController _owner;
        [SerializeField] private LangValueData _description;
        [SerializeField] private LangValueData _extendedDescription;

        public Image Image { get; private set; }
        public LangValueData Description => _description;
        public LangValueData ExtendedDescription => _extendedDescription;
        
        private void Start()
        {
            Image = GetComponent<Image>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _owner.ExhibitImageSelected?.Invoke(this);
        }
    }
}