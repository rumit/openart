﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class DignityFestInfo : ExtendedExhibitDisplay 
    {
        [SerializeField] private ViewerContainer _container;
        [SerializeField] private DignityFestConfig _config;
        [SerializeField] private TextMeshProUGUI _textField;
        
        private LangSelector _langSelector;

        [Inject]
        private void Inject(LangSelector langSelector)
        {
            _langSelector = langSelector;
        }
        
        private void Awake()
        {
            _container.SetActive(false);
        }

        public override void Show()
        {
            _container.SetActive(true);
            _textField.text = _langSelector.GetLocalText(_config.Info);
        }
        
        public override void Hide()
        {
            _container.SetActive(false);
        }
    }
}