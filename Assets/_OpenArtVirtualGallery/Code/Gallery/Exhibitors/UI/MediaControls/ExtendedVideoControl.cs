﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using TMPro;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class ExtendedVideoControl : VideoControl
    {
        [SerializeField] private TextMeshProUGUI _videoNameField;
        [SerializeField] private TextMeshProUGUI _authorNameField;

        public void Init(ExhibitorConfig.ArtVideoData data, string projectName, string authorName,
            VideoViewer videoViewer, Action<MediaControl> startPlayingCallback,
            Action stoppedCallback)
        {
            
            base.Init(data, videoViewer, startPlayingCallback, stoppedCallback);
            _videoNameField.text = projectName;
            _authorNameField.text = authorName.Replace("<br>", $", ");
        }
    }
}