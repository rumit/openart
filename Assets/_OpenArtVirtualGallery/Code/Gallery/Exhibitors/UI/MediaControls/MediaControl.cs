﻿using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public abstract class MediaControl : MonoBehaviour
    {
        [SerializeField] protected GameObject _selectionLine;
        [SerializeField] protected Button _stopBtn;
        [SerializeField] protected TwoStatesButton _playPauseBtn;
        
        protected Action<MediaControl> _playStateCallback;
        protected Image _bground;

        public abstract bool isPlaying { get; }

        protected virtual void OnDestroy()
        {
            _playPauseBtn.onValueChanged.RemoveListener(OnPlayPauseBtnClick);
            _stopBtn.onClick.RemoveListener(OnStopBtnClick);
        }
        
        protected virtual void Awake()
        {
            _bground = GetComponent<Image>();
            DrawPlayState(false);
            
            _playPauseBtn.onValueChanged.AddListener(OnPlayPauseBtnClick);
            _stopBtn.onClick.AddListener(OnStopBtnClick);
        }

        protected void Init(Action<MediaControl>playStateCallback)
        {
            _playStateCallback = playStateCallback;
        }

        public abstract void Play();

        public virtual void Stop()
        {
            OnStopBtnClick();
        }
        
        protected virtual void OnStopBtnClick()
        {
            if(!_playPauseBtn.isOn)
                return;
            
            DrawPlayState(false);
            _playPauseBtn.isOn = false;
        }
        
        protected virtual void OnPlayPauseBtnClick(bool value)
        {
            if (value)
            {
                _playStateCallback?.Invoke(this);
                DrawPlayState(true);
            }
            else
                DrawPlayState(false);
        }
        
        protected void DrawPlayState(bool value)
        {
            if (value)
            {
                _bground.DOFade(1, 0);
                _selectionLine.SetActive(true);
            }
            else
            {
                _bground.DOFade(0.25f, 0);
                _selectionLine.SetActive(false);
            }
        }
    }
}