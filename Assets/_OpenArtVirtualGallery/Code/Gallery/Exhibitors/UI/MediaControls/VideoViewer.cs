﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;
using static UnityEngine.UI.AspectRatioFitter;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class VideoViewer : MonoBehaviour
    {
        public event Action Complete;
        public event Action Ready;
        
        [SerializeField] private GameObject _container;
        [SerializeField] private GameObject _bufferingIcon;
        [SerializeField] private VideoPlayer _player;

        private AppConfig.URLs _urls;
        private ScenesManager _scenesManager;
        private bool _isDefaultLooping;
        private bool _isSplashScreen;
        //private AspectRatioFitter _aspectRatioFitter;

        public string CurrentUrl { get; private set; }
        public bool IsPlaying => _player.isPlaying || _player.waitForFirstFrame;
        public bool IsPaused => _player.isPaused;

        [Inject]
        private void Inject(AppConfig.URLs urls, ScenesManager scenesManager)
        {
            _urls = urls;
            _scenesManager = scenesManager;
        }

        private void OnDestroy()
        {
            _player.prepareCompleted -= OnPreparedCompleted;
            _player.loopPointReached -= OnComplete;
        }

        private void Awake()
        {
            _player.targetTexture.Release();
            _isDefaultLooping = _player.isLooping;
            SetVisibility(false);
            _bufferingIcon.SetActive(false);
            //_aspectRatioFitter = _player.GetComponent<AspectRatioFitter>();
        }
        
        private void SetVisibility(bool value) => _container.SetActive(value);

        public void Play(string url, bool isSplashScreen)
        {
            _isSplashScreen = isSplashScreen;
            _player.isLooping =  _isSplashScreen || _isDefaultLooping;
            
            CurrentUrl = url;
            SetVisibility(true);
            _bufferingIcon.SetActive(true);

            var videosFolder = _scenesManager.Current.name.Split("_")[1];
            _player.url = _urls.VideosUrl + videosFolder + "/" + url;
            _player.prepareCompleted += OnPreparedCompleted;
            _player.Prepare();
        }

        public void Pause() => _player.Pause();

        public void Resume() => _player.Play();

        public void Stop()
        {
            _player.Stop();
            _player.loopPointReached -= OnComplete;
            _player.targetTexture.Release();
            SetVisibility(false);
            _bufferingIcon.SetActive(false);
        }
        
        private void OnPreparedCompleted(VideoPlayer source)
        {
            _player.prepareCompleted -= OnPreparedCompleted;
            _player.loopPointReached += OnComplete;
            _bufferingIcon.SetActive(false);

            //var tex = _player.texture;
            //_aspectRatioFitter.aspectMode = tex.width > tex.height ? AspectMode.EnvelopeParent : AspectMode.FitInParent;
            //_aspectRatioFitter.aspectRatio = 1;

            _player.Play();
            Ready?.Invoke();
        }

        private void OnComplete(VideoPlayer source)
        {
            if(_isSplashScreen)
                return;
            
            _player.loopPointReached -= OnComplete;
            Complete?.Invoke();
        }
    }
}