﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class AudioControl : MediaControl
    {
        private AudioSource _audioSrc;
        private CancellationTokenSource _cancelationToken;
        private float _timeRemaining = -1;

        public override bool isPlaying => _audioSrc.isPlaying;

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _cancelationToken?.Cancel();
        }

        protected override void Awake()
        {
            base.Awake();
            _audioSrc = GetComponent<AudioSource>();
        }

        public void Init(AudioClip audioClip, Action<MediaControl> playStateCallback)
        {
            _audioSrc.clip = audioClip;
            base.Init(playStateCallback);
        }
        
        public override void Play()
        {
            _timeRemaining = _audioSrc.clip.length - _audioSrc.time;
            _cancelationToken?.Cancel();
            _cancelationToken = new CancellationTokenSource();
            UniTask.Delay(TimeSpan.FromSeconds(_timeRemaining), cancellationToken:_cancelationToken.Token)
                .ContinueWith(OnStopBtnClick);
            _audioSrc.Play();
        }

        protected override void OnStopBtnClick()
        {
            base.OnStopBtnClick();
            _audioSrc.Stop();
            _timeRemaining = -1;
        }

        protected override void OnPlayPauseBtnClick(bool value)
        {
            base.OnPlayPauseBtnClick(value);
            if (value)
                Play();
            else
                _audioSrc.Pause();
        }
    }
}