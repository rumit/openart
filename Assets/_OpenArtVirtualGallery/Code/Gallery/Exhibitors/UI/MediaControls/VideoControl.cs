﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class VideoControl : MediaControl
    {
        public override bool isPlaying => _videoViewer.IsPlaying;

        private Action _stoppedCallback;
        private VideoViewer _videoViewer;
        private string _url;
        
        public bool IsSplashScreen { get; private set; }

        protected override void OnDestroy()
        {
            OnStopBtnClick();
            base.OnDestroy();
        }

        public void Init(string url, VideoViewer videoViewer, Action<MediaControl> startPlayingCallback, Action stoppedCallback)
        {
            _url = url;
            _videoViewer = videoViewer;
            _stoppedCallback = stoppedCallback;

            base.Init(startPlayingCallback);
        }

        protected void Init(ExhibitorConfig.ArtVideoData data, VideoViewer videoViewer, Action<MediaControl> startPlayingCallback, Action stoppedCallback)
        {
            _url = data.Url;
            IsSplashScreen = data.IsSplashScreen;
            _videoViewer = videoViewer;
            _stoppedCallback = stoppedCallback;

            base.Init(startPlayingCallback);
        }

        public override void Play() => _playPauseBtn.isOn = true;

        public override void Stop()
        {
            if (_url == _videoViewer.CurrentUrl)
                OnStopBtnClick();
            else
            {
                base.OnStopBtnClick();
                _videoViewer.Complete -= OnStopBtnClick;
                _playPauseBtn.interactable = true;
                _stoppedCallback?.Invoke();
            }
        }

        protected override void OnStopBtnClick()
        {
            if(_url != _videoViewer.CurrentUrl)
                return;
            
            base.OnStopBtnClick();

            _videoViewer.Complete -= OnStopBtnClick;
            _videoViewer.Stop();
            _playPauseBtn.interactable = true;
            _stoppedCallback?.Invoke();
        }
        
        protected override void OnPlayPauseBtnClick(bool value)
        {
            base.OnPlayPauseBtnClick(value);
            //Logger.Log($"OnPlayPauseBtnClick {value}");
            if (value)
            {
                _videoViewer.Complete += OnStopBtnClick;

                if (_videoViewer.IsPaused)
                    _videoViewer.Resume();
                else
                {
                    _playPauseBtn.interactable = false;
                    _videoViewer.Ready += OnVideoReady;
                    _videoViewer.Play(_url, IsSplashScreen);
                }
            }
            else
                _videoViewer.Pause();
        }

        protected void OnVideoReady() => _playPauseBtn.interactable = true;
    }
}