﻿using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.Exhibitors
{
    public class DignityFestInfoLauncher : MonoBehaviour
    {
        private GalleryInitializer _galleryInitializer;
        private UserTeleportsManager _userTeleportsManager;
        private Button _button;

        [Inject]
        private void Inject(GalleryInitializer galleryInitializer, UserTeleportsManager userTeleportsManager)
        {
            _galleryInitializer = galleryInitializer;
            _userTeleportsManager = userTeleportsManager;
        }

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            if(_userTeleportsManager.IsPlayerOnSecondLevel)
                return;
            
            _galleryInitializer.ExhibitDisplay.ShowModule<DignityFestInfo>();
        }
    }
}