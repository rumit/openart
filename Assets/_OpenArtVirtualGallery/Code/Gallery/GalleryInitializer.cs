﻿using System;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;
using Newtonsoft.Json;
using OpenArtVirtualGallery.Gallery.Chat;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Gallery.Exhibitors;
using Sirenix.Utilities;
using static AppConfig;

namespace OpenArtVirtualGallery.Gallery
{
    public class GalleryInitializer : MonoBehaviour
    {
        public ExhibitDisplay ExhibitDisplay => _exhibitDisplaysUI.GetComponent<ExhibitDisplay>();
        
        private DiContainer _diContainer;
        private ExhibitionsSceneConfig _config;
        private HeroConfigsList _heroConfigs;
        private VisitorPrefabs _visitorPrefabs;
        private ChatManager _chatManager;
        private GameObject _exhibitDisplaysUI;
        
#if UNITY_ANDROID || UNITY_IOS
        public IJoystick Joystick { get; private set; }
#endif

        [Inject]
        private void Inject(DiContainer diContainer, 
            ExhibitionsSceneConfig config, 
            HeroConfigsList heroConfigs,
            VisitorPrefabs visitorPrefabs,
            ChatManager chatManager)
        {
            _diContainer = diContainer;
            _config = config;
            _heroConfigs = heroConfigs;
            _visitorPrefabs = visitorPrefabs;
            _chatManager = chatManager;
        }

        private void Start()
        {
            _diContainer.InstantiatePrefab(_config.BotSpeechBaloonUIRef, _config.UIRoot);
            _exhibitDisplaysUI = _diContainer.InstantiatePrefab(_config.ExhibitDisplaysUIRef, _config.UIRoot);

#if UNITY_ANDROID || UNITY_IOS
            Joystick = _diContainer.InstantiatePrefab(_config.JoystickRef, _config.UIRoot).GetComponent<IJoystick>();
#endif

            InitPlayerHero();
             
             if(PhotonNetwork.IsMasterClient)
                 InitBots();
             
             _chatManager.Initialize();
        }

        private void InitPlayerHero()
        {
            var pos = _config.EntryPoint.GetPosition();
            var randX = pos.x + (.1f + Random.value * .1f);
            var randZ = pos.z + (.1f + Random.value * .1f);
            pos = new Vector3(randX, pos.y, randZ);

            var rot = _config.EntryPoint.GetRotation();
            var localPlayerPrefab = _visitorPrefabs.GetHeroPrefab();
            PhotonNetwork.Instantiate(localPlayerPrefab.name, pos, rot);
        }
        
        private void InitBots()
        {
            if(_config.SpawnBotsAreas.IsNullOrEmpty())
                return;

            var heroConfigs = _heroConfigs.Get();
            var indexes = heroConfigs.GetRandomElements(_config.SpawnBotsAreas.Length)
                .Select(c => heroConfigs.IndexOf(c)).ToList();
            
            for (var i = 0; i < _config.SpawnBotsAreas.Length; i++)
            {
                var spawnArea = _config.SpawnBotsAreas[i];
                var prefabName = _visitorPrefabs.GetBotVisitorPrefab().name;
                
                var pos = spawnArea.GetStartPointInArea();
                var path = spawnArea.GetPath(pos);
                var serializedPath = JsonConvert.SerializeObject(path, Formatting.Indented,
                    new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
                var data = new object[] {indexes[i], serializedPath, spawnArea.Floor.ToString()};
                PhotonNetwork.InstantiateRoomObject(prefabName, pos, Quaternion.identity, 0, data);
            }
        }
    }
}