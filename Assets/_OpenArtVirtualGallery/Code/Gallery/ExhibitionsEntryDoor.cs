﻿using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Visitor;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace OpenArtVirtualGallery.Gallery
{
    public class ExhibitionsEntryDoor : MonoBehaviour
    {
        [SerializeField] private ScenesManager.Scenes _scene;
        [SerializeField] private TextMeshPro _titleField;
        [SerializeField] private TextMeshPro _exhibitionContinuationField;

        private AppConfig.Common _commonConfig;
        private ExhibitionsSceneConfig _sceneConfig;
        private JoinRoomController _joinRoomController;
        private AppConfig.CommonAppPrefabs _commonAppPrefabs;
        private VisitorsManager _visitorsManager;
        private LangValuesConfig<LangBaseValuesConfig.BaseLangIds>.LangValues _langValues;
        private PlayerMotionController _playerMotionController;
        private Vector3 _mouseDownPosition;
        private BoxCollider[] _colliders;

        [Inject]
        private void Inject(JoinRoomController joinRoomController,
            AppConfig.CommonAppPrefabs commonAppPrefabs,
            AppConfig.Common commonConfig,
            ExhibitionsSceneConfig sceneConfig,
            VisitorsManager visitorsManager,
            LangValuesConfig<LangBaseValuesConfig.BaseLangIds>.LangValues langValues)
        {
            _commonConfig = commonConfig;
            _joinRoomController = joinRoomController;
            _commonAppPrefabs = commonAppPrefabs;
            _sceneConfig = sceneConfig;
            _visitorsManager = visitorsManager;
            _langValues = langValues;
        }

        private async void Start()
        {
            _colliders = GetComponents<BoxCollider>();
            _titleField.text = _commonConfig.GetExibitionName(_scene);
            
            var subtitle = _langValues.Get(LangBaseValuesConfig.BaseLangIds.ExhibitionContinuationMessage);
            _exhibitionContinuationField.text = subtitle;
            
            var playerVisitor = await _visitorsManager.GetLocalPlayerHeroAsync();
            _playerMotionController = playerVisitor.GetComponent<PlayerMotionController>();
        }

        public void OnMouseDown()
        {
            if(!enabled || EventSystem.current.IsPointerOverGameObject())
                return;
            _mouseDownPosition = Input.mousePosition;
        }

        private void OnMouseUpAsButton()
        {
            if(!enabled || EventSystem.current.IsPointerOverGameObject() || !Input.mousePosition.Equals(_mouseDownPosition))
                return;
        
            ToggleDoorInteractable(false);

#if UNITY_WEBGL
            if(_playerMotionController)
                _playerMotionController.ToogleLookEnabled(false);
#endif
            
            var popup = _commonAppPrefabs.GetDialogPopupPrefab();
            var message = _langValues.Get(LangBaseValuesConfig.BaseLangIds.ExhibitionEntranceMessage, _commonConfig.GetExibitionName(_scene));
            ModalPopupsManager.ShowPopup(popup, _sceneConfig.UIRoot, message);
            ModalPopupsManager.Current.ActionSelected += OnActionSelected;
            
            _mouseDownPosition = default;
        }

        private void ToggleDoorInteractable(bool value)
        {
            foreach (var collider in _colliders)
                collider.enabled = value;
        }

        private async void OnActionSelected(bool value, IPopup popup)
        {
            ModalPopupsManager.Current.ActionSelected -= OnActionSelected;
            await ModalPopupsManager.RemovePopup();

            if (value)
                _joinRoomController.JoinRoom(_scene.ToString());
            else
            {
                ToggleDoorInteractable(true);
#if UNITY_WEBGL
                _playerMotionController.ToogleLookEnabled(true);
#endif
            }
        }
    }
}