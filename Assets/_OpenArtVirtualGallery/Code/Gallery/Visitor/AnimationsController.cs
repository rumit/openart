﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using Photon.Pun;
using Sirenix.Utilities;
using UnityEngine;
using Utils;
using static OpenArtVirtualGallery.Visitor.PlayerMotionController;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery.Visitor
{
    public class AnimationsController : MonoBehaviour
    {
        private static readonly Dictionary<Animations, int> _animationsHash = new Dictionary<Animations, int>();

        public enum Animations
        {
            Idle,
            Walking,
            StepLeft,
            StepRight,
            WalkingBackwards
        }

        private Animator _animator;
        private AbstractVisitor _owner;

#if ENABLE_INPUT_SYSTEM && UNITY_WEBGL
        private PlayerInputActions _inputActions;
#endif
        private PlayerMotionController _playerMotionController;
        //private string[] _animationsStrings;

        private void Awake()
        {
            //_animationsStrings = Enum.GetNames(typeof(Animations));
            if (_animationsHash.Count > 0) 
                return;
            
            _animationsHash.Add(Animations.Idle, Animator.StringToHash(Animations.Idle.ToString()));
            _animationsHash.Add(Animations.Walking, Animator.StringToHash(Animations.Walking.ToString()));
            _animationsHash.Add(Animations.StepLeft, Animator.StringToHash(Animations.StepLeft.ToString()));
            _animationsHash.Add(Animations.StepRight, Animator.StringToHash(Animations.StepRight.ToString()));
            _animationsHash.Add(Animations.WalkingBackwards, Animator.StringToHash(Animations.WalkingBackwards.ToString()));
        }
        
        private void OnDestroy()
        {
#if ENABLE_INPUT_SYSTEM && UNITY_WEBGL
            if(_inputActions != null)
                _inputActions.MoveAction -= OnMoveAction;
#endif
        }

        public void Init(AbstractVisitor owner)
        {
            _owner = owner;
            
            _animator = GetComponent<Animator>();
            _animator.runtimeAnimatorController = _owner.Config.Animator;
            _animator.avatar = _owner.Config.AnimatorAvatar;
            
            var photonAnimatorView = GetComponent<PhotonAnimatorView>();
            photonAnimatorView.SetLayerSynchronized(0, PhotonAnimatorView.SynchronizeType.Discrete);
            photonAnimatorView.SetParameterSynchronized(Animations.Idle.ToString(), PhotonAnimatorView.ParameterType.Bool,
                PhotonAnimatorView.SynchronizeType.Discrete);
            photonAnimatorView.SetParameterSynchronized(Animations.Walking.ToString(), PhotonAnimatorView.ParameterType.Bool,
                PhotonAnimatorView.SynchronizeType.Discrete);
            photonAnimatorView.SetParameterSynchronized(Animations.WalkingBackwards.ToString(), PhotonAnimatorView.ParameterType.Bool,
                PhotonAnimatorView.SynchronizeType.Discrete);
            photonAnimatorView.SetParameterSynchronized(Animations.StepLeft.ToString(), PhotonAnimatorView.ParameterType.Bool,
                PhotonAnimatorView.SynchronizeType.Discrete);
            photonAnimatorView.SetParameterSynchronized(Animations.StepRight.ToString(), PhotonAnimatorView.ParameterType.Bool,
                PhotonAnimatorView.SynchronizeType.Discrete);
        }

        public void InitLocalPlayer()
        {
            _playerMotionController = GetComponent<PlayerMotionController>();
#if UNITY_WEBGL
            _inputActions = GetComponent<PlayerInputActions>();
            _inputActions.MoveAction += OnMoveAction;
#endif
        }

#if UNITY_WEBGL
        private void OnMoveAction(bool value)
        {
            var dir = _playerMotionController.GetMoveDirection(new Vector2Int((int)_inputActions.Move.x, (int)_inputActions.Move.y));
            switch (dir)
            {
                case PlayerMotionController.MoveDirections.None: PlayAnimation(Animations.Idle); break;
                case PlayerMotionController.MoveDirections.Forward: PlayAnimation(Animations.Walking); break;
                case PlayerMotionController.MoveDirections.Back: PlayAnimation(Animations.WalkingBackwards); break;
                case PlayerMotionController.MoveDirections.Leftward: PlayAnimation(Animations.StepLeft); break;
                case PlayerMotionController.MoveDirections.Rightward: PlayAnimation(Animations.StepRight); break;
            }
        }
#endif
        
        public void PlayAnimation(Animations animationType)
        {
            var animId = _animationsHash[animationType];
            if (_playerMotionController != null)
                _playerMotionController.MoveSpeed = animationType == Animations.StepLeft || animationType == Animations.StepRight ? 0.65f : 2;

            //var current = animationType.ToString();
            foreach (var id in _animationsHash.Values.Where(id => id != animId))
            {
                _animator.SetBool(id, false);
            }
            // Array.ForEach(_animationsStrings, a =>
            // {
            //     if(a != current) _animator.SetBool(a, false);
            // });

            _animator.SetBool(animId, true);
        }
    }
}