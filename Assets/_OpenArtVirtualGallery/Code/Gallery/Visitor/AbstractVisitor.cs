﻿using OpenArtVirtualGallery.User;
using Photon.Pun;
using UnityEngine;
using Zenject;
using static OpenArtVirtualGallery.Visitor.AnimationsController;

namespace OpenArtVirtualGallery.Visitor
{
    [RequireComponent(typeof(PhotonView), typeof(PhotonTransformViewClassic), typeof(ZenAutoInjecter))]
    [RequireComponent(typeof(AnimationsController))]
    public abstract class AbstractVisitor : MonoBehaviour, IPunInstantiateMagicCallback
    {
        [Inject] protected AppConfig.HeroConfigsList _heroConfigs;
        [Inject] protected VisitorsManager _visitorsManager;

        public VisitorHeroModel Model { get; protected set; }
        public VisitorConfig Config { get; protected set; }
        public PhotonView PhotonView { get; protected set; }
        
        protected AnimationsController _animationsController;
        protected BodyController _bodyController;
        
        public float Radius { get; protected set; }
        
        protected virtual void Awake() => _animationsController = GetComponent<AnimationsController>();
        
        protected virtual void OnDestroy() => _visitorsManager.Remove(this);

        protected void AddToVisitorsManager() => _visitorsManager.Add(this);

        protected void InitAdditionalControllers()
        {
            _bodyController = Instantiate(Config.HeroModelPrefabRef, transform);
            _bodyController.Init(this);
            _animationsController.Init(this);
        }

        public virtual void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            PhotonView = info.photonView;
        }
        
        public override string ToString()
        {
            return $"[{Model}]";
        }
    }
}