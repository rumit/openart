﻿using System;
using System.Linq;
using DG.Tweening;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;
using static AppConfig;
using Logger = OpenArtVirtualGallery.Utils.Logger;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

/* Note: animations are called via the controller for both the character and capsule using animator null checks*/
namespace OpenArtVirtualGallery.Visitor
{
	[RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM && UNITY_WEBGL
	[RequireComponent(typeof(PlayerInput))]
#endif
	public class PlayerMotionController : MonoBehaviour
	{
		public enum MoveDirections
		{
			None,
			Forward,
			Back,
			Leftward,
			Rightward
		}
		
		[Header("Player")]
		[Tooltip("Move speed of the character in m/s")]
		public float MoveSpeed;
		[Tooltip("Rotation speed of the character")]
		public float RotationSpeed = 1.0f;
		[Tooltip("Acceleration and deceleration")]
		public float SpeedChangeRate = 10.0f;

		[Header("Player Grounded")]
		[Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
		public bool Grounded = true;
		[Tooltip("Useful for rough ground")]
		public float GroundedOffset = -0.14f;
		[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
		public float GroundedRadius = 0.5f;
		[Tooltip("What layers the character uses as ground")]
		public LayerMask GroundLayers;
		
		[Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
		public float FallTimeout = 0.15f;
		
		public float Gravity = -15.0f;
		
		public float Radius { get; private set; }

		[Header("Cinemachine")]
		[Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
		public GameObject CinemachineCameraTarget;
		[Tooltip("How far in degrees can you move the camera up")]
		public float TopClamp = 90.0f;
		[Tooltip("How far in degrees can you move the camera down")]
		public float BottomClamp = -90.0f;
		
		public PlayerInputActions Input { get; private set; }

		// cinemachine
		private float _cinemachineTargetPitch;

		// player
#if UNITY_WEBGL
		private float _speed;
#endif
#if UNITY_ANDROID || UNITY_IOS
		private const float _speed = 3f;
#endif
		
		private float _rotationVelocity;
		private float _verticalVelocity;
		private float _terminalVelocity = 53.0f;

		private float _fallTimeoutDelta;

		private CharacterController _controller;
		private VisitorController _visitorController;
		private TeleportPointsChecker _teleportPointChecker;

		private const float _threshold = 0.01f;

		private void OnDestroy()
		{
#if UNITY_ANDROID || UNITY_IOS
			Input.JoystickMove -= OnJoystickMoveAction;
			Input.JoystickPan -= OnJoystickPanAction;
#endif
		}

		private void Start()
		{
			_controller = GetComponent<CharacterController>();
			Radius = _controller.radius;
			_visitorController = GetComponent<VisitorController>();
			_teleportPointChecker = GetComponent<TeleportPointsChecker>();
			_fallTimeoutDelta = FallTimeout;
			
			Input = GetComponent<PlayerInputActions>();
#if UNITY_ANDROID || UNITY_IOS
			Input.JoystickMove += OnJoystickMoveAction;
			Input.JoystickPan += OnJoystickPanAction;
#endif
		}
		
#if UNITY_ANDROID || UNITY_IOS
		private void OnJoystickMoveAction(Vector2 value)
		{
			_teleportPointChecker.CheckState();
			//Debug.Log($"OnMoveAction: {value}");

			var direction = transform.forward * (_speed * Time.deltaTime);
			_controller.Move(direction + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
		}
		
		private void OnJoystickPanAction(float angle)
		{
			transform.DORotate(new Vector3(0, angle, 0), .2f);
		}
#endif
		
		private void Update()
		{
			HandleGravity();
			GroundedCheck();
#if UNITY_WEBGL
			Move();
#endif
		}

		private void GroundedCheck()
		{
			// set sphere position, with offset
			var pos = transform.position;
			var spherePosition = new Vector3(pos.x, pos.y - GroundedOffset, pos.z);
			Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
		}

		private void HandleGravity()
		{
			if (Grounded)
			{
				// reset the fall timeout timer
				_fallTimeoutDelta = FallTimeout;

				// stop our velocity dropping infinitely when grounded
				if (_verticalVelocity < 0.0f)
					_verticalVelocity = -2f;
			}
			else
			{
				// fall timeout
				if (_fallTimeoutDelta >= 0.0f)
					_fallTimeoutDelta -= Time.deltaTime;
			}

			// apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
			if (_verticalVelocity < _terminalVelocity)
				_verticalVelocity += Gravity * Time.deltaTime;
		}

#if UNITY_WEBGL
		private void LateUpdate() => CameraRotation();
		
		private void CameraRotation()
		{
			// if there is an input
			if (!(Input.Look.sqrMagnitude >= _threshold)) 
				return;
			
			_cinemachineTargetPitch += Input.Look.y * RotationSpeed * Time.deltaTime;
			_rotationVelocity = Input.Look.x * RotationSpeed * Time.deltaTime;

			// clamp our pitch rotation
			_cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);
			
			// Update Cinemachine camera target pitch
			CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);

			// rotate the player left and right
			transform.Rotate(Vector3.up * _rotationVelocity);
		}

		private void Move()
		{
			_teleportPointChecker.CheckState();
			// set target speed based on move speed, sprint speed and if sprint is pressed
			var targetSpeed = MoveSpeed;
			// a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

			// note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is no input, set the target speed to 0
			if (Input.Move == Vector2.zero) targetSpeed = 0.0f;

			// a reference to the players current horizontal velocity
			var currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

			var speedOffset = 0.1f;
			var inputMagnitude = Input.analogMovement ? Input.Move.magnitude : 1f;

			// accelerate or decelerate to target speed
			if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
			{
				// creates curved result rather than a linear one giving a more organic speed change
				// note T in Lerp is clamped, so we don't need to clamp our speed
				_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);
				// round speed to 3 decimal places
				_speed = Mathf.Round(_speed * 1000f) / 1000f;
			}
			else
			{
				_speed = targetSpeed;
			}

			// normalise input direction
			var inputDirection = new Vector3(Input.Move.x, 0.0f, Input.Move.y).normalized;

			// note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is a move input rotate player when the player is moving
			if (Input.Move != Vector2.zero)
			{
				inputDirection = transform.right * Input.Move.x + transform.forward * Input.Move.y;
			}
			
			// move the player
			//_controller.Move(inputDirection.normalized * (_speed * Time.deltaTime));
			_controller.Move(inputDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
		}
		
		public MoveDirections GetMoveDirection(Vector2Int value)
		{
			if (Input.Move == Vector2.zero)
				return MoveDirections.None;
            
			switch (value.y)
			{
				case 1: return MoveDirections.Forward;
				case -1: return MoveDirections.Back;
			}
            
			switch (value.x)
			{
				case 1: return MoveDirections.Rightward;
				case -1: return MoveDirections.Leftward;
			}
            
			return MoveDirections.None;
		}
		
		private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
		{
			if (lfAngle < -360f) lfAngle += 360f;
			if (lfAngle > 360f) lfAngle -= 360f;
			return Mathf.Clamp(lfAngle, lfMin, lfMax);
		}
#endif

		public void Teleport(Vector3 targetPoint)
		{
			var camTransform = CinemachineCameraTarget.transform;
			var camPos = camTransform.localPosition;
			var camRot = camTransform.localRotation;
			var camTargetPoint = targetPoint + Vector3.up * _visitorController.Config.CameraPosition.y;

			camTransform.DOMove(camTargetPoint, .2f)
				.OnComplete(() =>
				{
					_controller.enabled = false;
					transform.position = targetPoint;
					camTransform.localPosition = camPos;
					camTransform.localRotation = camRot;
					_controller.enabled = true;
				});
		}

#if UNITY_WEBGL
				public void ToogleLookEnabled(bool value) => Input.ToggleLookEnabled(value);
#endif

		private void OnDrawGizmosSelected()
		{
			var transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
			var transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);
		
			if (Grounded) Gizmos.color = transparentGreen;
			else Gizmos.color = transparentRed;
		
			// when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
			Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z), GroundedRadius);
		}
	}
}