﻿using System;

using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Gallery.GalleryLevel;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;
using static LangBaseValuesConfig;

namespace OpenArtVirtualGallery.Visitor
{
    public class UserTeleportsManager : IInitializable, IDisposable
    {
        private readonly VisitorsManager _visitorsManager;
        private readonly UserActions _userActions;
        private readonly ExhibitionsSceneConfig _sceneConfig; 
        private readonly LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;
        
        private VisitorController _playerVisitor;
        private PlayerMotionController _playerMotionController;
        private Vector3? _supposedTeleportPoint;
        private readonly AppConfig.CommonAppPrefabs _commonAppPrefabs;

        public UserTeleportsManager(VisitorsManager visitorsManager,
            AppConfig.CommonAppPrefabs commonAppPrefabs,
            UserActions userActions, 
            ExhibitionsSceneConfig sceneConfig,
            LangValuesConfig<BaseLangIds>.LangValues langBaseValues)
        {
            _visitorsManager = visitorsManager;
            _commonAppPrefabs = commonAppPrefabs;
            _userActions = userActions;
            _sceneConfig = sceneConfig;
            _langBaseValues = langBaseValues;
        }
        
        public bool IsPlayerOnSecondLevel { get; private set; }

        public void Initialize()
        {
            _userActions.EntryTeleportArea += OnEntryTeleportArea;
            _userActions.LeaveTeleportArea += OnLeaveTeleportArea;
        }

        public void Dispose()
        {
            _userActions.EntryTeleportArea -= OnEntryTeleportArea;
            _userActions.LeaveTeleportArea -= OnLeaveTeleportArea;
        }

        private void OnEntryTeleportArea(LevelsTeleportEntry teleportArea)
        {
            _playerVisitor = _visitorsManager.GetLocalPlayerHero();
            _playerMotionController = _playerVisitor.GetComponent<PlayerMotionController>();
            _supposedTeleportPoint = teleportArea.GetExitPoint();
#if UNITY_WEBGL
            _playerMotionController.ToogleLookEnabled(false);
#endif
            var popupPrefab = _commonAppPrefabs.GetDialogPopupPrefab();
            var message = _playerVisitor.Model.LegalAgeConfirmed
                ? teleportArea.Location == LevelsTeleportEntry.Locations.Bottom 
                    ? $"{_langBaseValues.Get(BaseLangIds.GoUp)}"
                    : $"{_langBaseValues.Get(BaseLangIds.GoDown)}"
                : "18+?";
            ModalPopupsManager.ShowPopup(popupPrefab, _sceneConfig.UIRoot, message);
            ModalPopupsManager.Current.ActionSelected += OnActionSelected;
        }

        private void OnActionSelected(bool value, IPopup popup)
        {
            if (value)
            {
                _playerVisitor.Model.LegalAgeConfirmed = true;
                if (_supposedTeleportPoint != null)
                {
                    var p = (Vector3) _supposedTeleportPoint;
                    IsPlayerOnSecondLevel = p.y > .3f;
                    _playerMotionController.Teleport(p);
                }
            }

            RemovePopup();
        }
        
        private void OnLeaveTeleportArea() => RemovePopup();

        private void RemovePopup()
        {
            _supposedTeleportPoint = null;
            ModalPopupsManager.RemovePopup();
#if UNITY_WEBGL
            _playerMotionController.ToogleLookEnabled(true);
#endif
        }
    }
}