﻿using System;
using System.Linq;
using OpenArtVirtualGallery.Gallery.GalleryLevel;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Visitor
{
    public class TeleportPointsChecker : MonoBehaviour
    {
        [Inject] private UserActions _userActions;
        
        private Collider[] _obstacles = new Collider[2];
        private bool _inTeleportArea;

        public void CheckState()
        {
            var pos = transform.position + Vector3.up;
            Physics.OverlapSphereNonAlloc(pos, 0.6f, _obstacles);
            var teleportAreaCollider =
                _obstacles.FirstOrDefault(c => c != null && c.gameObject.layer == (int) AppConfig.Layers.TeleportEntry);
            
            if(teleportAreaCollider != null)
            {
                if (_inTeleportArea)
                    return;
                
                _inTeleportArea = true;
                var teleportEntry = teleportAreaCollider.GetComponent<LevelsTeleportEntry>();
                _userActions.EntryTeleportArea?.Invoke(teleportEntry);
            }
            else
            {
                if (!_inTeleportArea) 
                    return;
                _inTeleportArea = false;
                _userActions.LeaveTeleportArea?.Invoke();
            }
        }
    }
}