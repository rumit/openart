﻿using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Visitor
{
	public class BotCollisionsController : MonoBehaviour
	{
		[Inject] private UserActions _userActions;
		[SerializeField] private float _maxAngle = 25f;
		[SerializeField] private float _exitCollisionDistance = 1.2f;
		
		private Transform _transform;
		private BotController _currentCollidedBot;

		private void Awake()
		{
			_transform = transform;
		}

		private void OnControllerColliderHit(ControllerColliderHit hit)
		{
			CheckBot(hit);
		}

		private void CheckBot(ControllerColliderHit hit)
		{
			var bot = hit.collider.GetComponent<BotController>();
			if(bot == null || _currentCollidedBot == bot)
				return;
			
			var angle = Vector3.Angle(_transform.forward, bot.transform.position - _transform.position);
			if(angle > _maxAngle || bot.AI.State == BotAI.MovementStates.Move)
				return;

			//Utils.Logger.Log($"collide with {bot}, angle: {angle}, state: {bot.AI.State}");
			_userActions.BotCollision?.Invoke(bot);
			
			UniTask.WaitUntil(() => Vector3.Distance(bot.transform.position, _transform.position) > _exitCollisionDistance)
				.ContinueWith(() =>
				{
					//Utils.Logger.Log($"collision exit {_currentCollidedBot}");
					_currentCollidedBot = null;
				});
			
			_currentCollidedBot = bot;
		}
	}
}
