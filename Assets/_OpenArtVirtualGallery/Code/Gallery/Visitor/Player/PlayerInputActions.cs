using System;
using OpenArtVirtualGallery.Gallery;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Zenject;

#if UNITY_WEBGL
using UnityEngine.InputSystem;
#endif

namespace OpenArtVirtualGallery.Visitor
{
	public class PlayerInputActions : MonoBehaviour
	{
		[Header("Movement Settings")] 
		public bool analogMovement;

		private UserActions _userActions;

#if UNITY_ANDROID || UNITY_IOS

		public Action<Vector2> JoystickMove;
		public event Action JoystickStopMove;
		public event Action<float> JoystickPan;

		private GalleryInitializer _galleryInitializer;
		private IJoystick _joystick;

		[Inject]
		private void Inject(UserActions userActions, GalleryInitializer galleryInitializer)
		{
			_userActions = userActions;
			_galleryInitializer = galleryInitializer;
		}
#endif
		
#if UNITY_WEBGL
		public event Action<bool> MoveAction;
		public Vector2 Move;
		public Vector2 Look;

		private bool _navigationEnabled = true;
		private bool _lookEnabled = true;
		private bool _moveEnabled = true;

		[Inject]
		private void Inject(UserActions userActions)
		{
			_userActions = userActions;
		}
#endif
		
		private void OnDestroy()
		{
#if UNITY_WEBGL
			_userActions.ChatInteraction -= OnChatInteraction;
			_userActions.ChatInputFieldTyping -= OnChatInputTyping;
#endif
			
			_userActions.TogglePlayerNavigation -= OnTogglePlayerNavigation;
			
#if UNITY_ANDROID || UNITY_IOS
			_joystick.MoveAction -= OnMoveAction;
			_joystick.PanAction -= OnJoystickPanAction;
#endif
		}
		
		private void Start()
		{
#if UNITY_WEBGL
			_userActions.ChatInteraction += OnChatInteraction;
			_userActions.ChatInputFieldTyping += OnChatInputTyping;
#endif
			
			_userActions.TogglePlayerNavigation += OnTogglePlayerNavigation;

#if UNITY_ANDROID || UNITY_IOS
			_joystick = _galleryInitializer.Joystick;
			_joystick.MoveAction += OnMoveAction;
			_joystick.PanAction += OnJoystickPanAction;
#endif
		}

#if UNITY_ANDROID || UNITY_IOS
		private void OnMoveAction(Vector2 value) => JoystickMove?.Invoke(value);

		private void OnJoystickPanAction(float angle) => JoystickPan?.Invoke(angle);
#endif

		private void OnTogglePlayerNavigation(bool value)
		{
#if UNITY_ANDROID || UNITY_IOS
			_joystick.Toggle(value);
			#endif
#if UNITY_WEBGL
			_navigationEnabled = value;
#endif
		}

#if UNITY_WEBGL		
		private void OnChatInputTyping(bool value) => ToggleMoveEnabled(!value);

		private void OnChatInteraction(bool value) => ToggleLookEnabled(!value);

		public void ToggleLookEnabled(bool value)
		{
			_lookEnabled = value;

			if (!value) Look = Vector2.zero;

		}

		public void ToggleMoveEnabled(bool value)
		{
			_moveEnabled = value;
			if (!value) Move = Vector2.zero;

		}

		public void OnMove(InputValue value)
		{
			if(!_navigationEnabled || !_moveEnabled)
				return;
			
			var vec = value.Get<Vector2>();
			if ((vec.x > 0 || vec.x < 0) && (vec.y > 0 || vec.y < 0))
			{
				Move = new Vector2();
				MoveAction?.Invoke(false);
				return;
			}
			MoveInput(vec);
		}

		public void OnLook(InputValue value)
		{
			if(!_navigationEnabled || !_lookEnabled)
				return;
			
			if (!Mouse.current.leftButton.isPressed)
			{
				Look = new Vector2();
				return;
			}
			LookInput(value.Get<Vector2>());
		}

		public void MoveInput(Vector2 newMoveDirection)
		{
			Move = newMoveDirection;
			MoveAction?.Invoke(Move != Vector2.zero);
		} 

		public void LookInput(Vector2 newLookDirection) => Look = newLookDirection;
		#endif
	}
}
