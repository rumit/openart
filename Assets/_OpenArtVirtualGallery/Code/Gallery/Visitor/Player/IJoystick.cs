﻿using System;
using UnityEngine;

namespace OpenArtVirtualGallery.Visitor
{
    public interface IJoystick
    {
        event Action<float> PanAction;
        event Action<Vector2> MoveAction;
        event Action StopMoveAction;
        public void Toggle(bool value);
    }
}