﻿using Newtonsoft.Json;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.User;
using Photon.Pun;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace OpenArtVirtualGallery.Visitor
{
    public class VisitorController : AbstractVisitor
    {
        [SerializeField] protected CapsuleCollider _internalCollider;

        private ExhibitionsSceneConfig _sceneConfig;
        public Transform CinemachineTarget { get; private set; }

        private NicknameDisplay _nicknameDisplay;

        [Inject]
        private void Inject(ExhibitionsSceneConfig sceneConfig)
        {
            _sceneConfig = sceneConfig;
        }

        protected override void Awake()
        {
            base.Awake();
            _internalCollider.enabled = false;
        }

        public override void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            base.OnPhotonInstantiate(info);
            
            gameObject.name = PhotonView.Controller.NickName;

            var data = (string)PhotonView.Controller.CustomProperties[PhotonView.Controller.UserId];
            Model = JsonConvert.DeserializeObject<VisitorHeroModel>(data);
            Debug.Log($"OnPhotonInstantiate UserId: {PhotonView.Controller.UserId}, Model: {Model}");
            Config = _heroConfigs.GetConfig(Model.Index);
            
            InitAdditionalControllers();
            InitNicknameDisplay();
            AddToVisitorsManager();
            
            if (PhotonView.IsMine)
                InitLocalHero();
            else
                InitNonLocalHero();
        }

        private void InitLocalHero()
        {
            _bodyController.HideOwnerBody();
            
            var cam = _sceneConfig.CinemachineVirtualCamera;
            cam.enabled = true;
            CinemachineTarget = GetComponentInChildren<CinemachineTarget>().transform;
            CinemachineTarget.localPosition = Config.CameraPosition;
            cam.Follow = CinemachineTarget;

            var motionController = GetComponent<PlayerMotionController>();
            Radius = motionController.Radius;
            Model.PlayerMotionController = motionController;
            
            _animationsController.InitLocalPlayer();
        }

        private void InitNonLocalHero()
        {
            Destroy(GetComponent<PlayerMotionController>());
            Destroy(GetComponent<TeleportPointsChecker>());
            Destroy(GetComponent<PlayerInput>());
            Destroy(GetComponent<CharacterController>());
            Destroy(GetComponent<PlayerInputActions>());
            Destroy(GetComponent<BotCollisionsController>());
            
            _internalCollider.enabled = true;
            Radius = _internalCollider.radius;
        }
        
        private void InitNicknameDisplay()
        {
            _nicknameDisplay = GetComponentInChildren<NicknameDisplay>();
            _nicknameDisplay.Init(Model.Name);
            _nicknameDisplay.transform.localPosition = Config.NicknameDisplayPosition;
        }
        
        public void UpdateNameDisplay(Transform target) => _nicknameDisplay.UpdateRotation(target);
    }
}