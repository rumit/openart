﻿using System;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Installers;
using TMPro;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Visitor
{
    public class NicknameDisplay : MonoBehaviour
    {
        [SerializeField] private AnimationCurve _scaleCurve;
        [SerializeField] private TextMeshProUGUI _textField;

        private ExhibitionsSceneConfig _config;
        private Transform _transform;
        private float _maxDist;

        [Inject]
        private void Inject(ExhibitionsSceneConfig config)
        {
            _config = config;
        }

        private void Awake()
        {
            _transform = transform;
            _maxDist = _config.MaxDistance;
        }

        public void Init(string name) => _textField.text = $"<u>{name}</u>";

        public void UpdateRotation(Transform target)
        {
            _transform.rotation = Quaternion.LookRotation(target.forward);
            
            var dist = Vector3.Distance(_transform.position, target.position);
            var perc = dist / _maxDist;

            var coeff = _scaleCurve.Evaluate(perc);
            _transform.localScale = Vector3.one * dist * coeff;
        }
    }
}