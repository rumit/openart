﻿using System.Collections.Generic;
using System.Linq;
using OpenArtVirtualGallery.Gallery.Configs;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Visitor.Bots
{
    public class BotSpawnArea : MonoBehaviour
    {
        [SerializeField] private Color _debugColor = Color.magenta;
        [SerializeField] private ExhibitionsSceneConfig.GalleryFloors _floor;
        
        [Space(10)]
        [SerializeField] private Vector2 _size;
        [SerializeField] private EditorPathPoint[] _pathPoints;
        
        private EditorPathPoint[] _path;

        public ExhibitionsSceneConfig.GalleryFloors Floor => _floor;
        public Vector3 GetStartPointInArea() => _pathPoints.First().Position;

        public List<PathPoint> GetPath(Vector3 startPos)
        {
            if(_pathPoints == null)
                return null;
            
            var result = new List<PathPoint>();
            for (var i = 0; i < _pathPoints.Length; i++)
            {
                var p = _pathPoints[i];
                var item = i == 0
                        ? new PathPoint {Position = startPos, LookTarget = p.GetLookTarget(), PresenceTime = p.PresenceTime}
                        : new PathPoint {Position = p.Position, LookTarget = p.GetLookTarget(), PresenceTime = p.PresenceTime};
                result.Add(item);
            }
            return result;
        }

        private void OnDrawGizmos()
        {
            if(_size.Equals(Vector2.zero))
                return;

            Gizmos.color = _debugColor;
            var halfWidth = _size.x / 2;
            var halfHeight = _size.y / 2;
            var pos = transform.position;
            var p0 = pos + new Vector3(-halfWidth, 0, -halfHeight);
            var p1 = pos + new Vector3(halfWidth, 0, -halfHeight);
            var p2 = pos + new Vector3(halfWidth, 0, halfHeight);
            var p3 = pos + new Vector3(-halfWidth, 0, halfHeight);
            Gizmos.DrawLine(p0, p1);
            Gizmos.DrawLine(p1, p2);
            Gizmos.DrawLine(p0, p3);
            Gizmos.DrawLine(p2, p3);
            
            if(_pathPoints == null)
                return;
            
            Gizmos.color = _debugColor;
            for (var i = 0; i < _pathPoints.Length; i++)
            {
                var p = _pathPoints[i];
                if(p == null)
                    continue;
                Gizmos.DrawSphere(p.Position, 0.1f);
                if (i < _pathPoints.Length - 1)
                    Gizmos.DrawLine(p.Position, _pathPoints[i + 1].Position);
            }
        }
    }
}