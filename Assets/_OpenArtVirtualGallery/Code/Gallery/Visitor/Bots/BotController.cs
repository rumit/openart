﻿using System;
using System.Collections.Generic;
using OpenArtVirtualGallery.Gallery.Configs;
using OpenArtVirtualGallery.Visitor;
using OpenArtVirtualGallery.User;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Newtonsoft.Json;
using OpenArtVirtualGallery.Installers;
using static OpenArtVirtualGallery.Installers.GallerySceneContextInstaller;

namespace OpenArtVirtualGallery.Gallery.Visitor.Bots
{
    public class BotController : AbstractVisitor, IPunObservable, IOnPhotonViewOwnerChange
    {
        [SerializeField] private BotConfig _botConfig;

        public BotAI AI { get; private set; }
        
        private ExhibitionsSceneConfig.GalleryFloors _floor;
        private PathPoint _aiTargetPoint;
        private BotAI.MovementStates _aiMovementState;

        protected override void OnDestroy()
        {
            base.OnDestroy();
            AI.Dispose();
            PhotonView.RemoveCallbackTarget(this);
        }

        public LangValueData GetRandomSpeech()
        {
            return _floor == ExhibitionsSceneConfig.GalleryFloors.First
                ? _botConfig.FirstFloorSpeechesConfig.GetRandomSpeech()
                : _botConfig.SecondFloorSpeechesConfig.GetRandomSpeech();
        }

        public override void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            base.OnPhotonInstantiate(info);
            
            PhotonView.AddCallbackTarget(this);
            
            var index = (int)PhotonView.InstantiationData[0];
            Model = new VisitorHeroModel(false) {Index = index, Name = $"Bot_{index}", LegalAgeConfirmed = true };
            gameObject.name = Model.Name;
            
            Config = _heroConfigs.GetConfig(Model.Index);
            Radius = GetComponent<CapsuleCollider>().radius;

            InitAdditionalControllers();
            AddToVisitorsManager();

            var serializedPath = (string) PhotonView.InstantiationData[1];
            var path = JsonConvert.DeserializeObject<List<PathPoint>>(serializedPath);
            AI = new BotAI(this, path, _botConfig.MoveSpeed);

            _floor = (ExhibitionsSceneConfig.GalleryFloors)Enum.Parse(typeof(ExhibitionsSceneConfig.GalleryFloors), (string) PhotonView.InstantiationData[2]);
            
            if(PhotonView.IsMine)
                AI.Start();
        }

        public void SaveTargetPoint(PathPoint targetPoint)
        {
            var serializedPoint = JsonConvert.SerializeObject(targetPoint, Formatting.None,
                new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            PhotonView.RPC(nameof(_SaveTargetPoint), RpcTarget.Others, serializedPoint);
        }
        
        public void SaveAIState(BotAI.MovementStates state)
        {
            PhotonView.RPC(nameof(_SaveAIState), RpcTarget.Others, state.ToString());
        }

        [PunRPC]
        private void _SaveTargetPoint(string p)
        {
            _aiTargetPoint = JsonConvert.DeserializeObject<PathPoint>(p);
        }
        
        [PunRPC]
        private void _SaveAIState(string state)
        {
            _aiMovementState = (BotAI.MovementStates)Enum.Parse(typeof(BotAI.MovementStates), state);
        }

        public void OnOwnerChange(Player newOwner, Player previousOwner)
        {
            if(!PhotonNetwork.IsMasterClient)
                return;
            AI.HandleOwnerChange(_aiTargetPoint, _aiMovementState);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }

        public override string ToString()
        {
            return $"[Name: {gameObject.name}, Floor: {_floor}]";
        }
    }
}