﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using ModestTree;
using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Logger = OpenArtVirtualGallery.Utils.Logger;
using Random = UnityEngine.Random;

namespace OpenArtVirtualGallery.Gallery.Visitor.Bots
{
    public class BotAI
    {
        public enum MovementStates
        {
            Idle,
            Move
        }
        
        private MovementStates _movementState = MovementStates.Idle;
        public MovementStates State
        {
            get => _movementState;
            private set
            {
                _movementState = value;
                _owner.SaveAIState(_movementState);
            }
        }

        private PathPoint _currentTargetPoint;
        private PathPoint CurrentTargetPoint
        {
            get => _currentTargetPoint;
            set
            {
                _currentTargetPoint = value;
                _targetPointIndex = _path.IndexOf(_currentTargetPoint);
                _owner.SaveTargetPoint(_currentTargetPoint);
            }
        }

        private readonly float _minTargetDistSqr = 0.01f * 0.01f;
        private float _minCollisionDistSqr;
        private LayerMask _visitorLayerMask;

        private BotController _owner;
        private Transform _transform;

        private List<PathPoint> _path;
        
        private int _targetPointIndex;

        private AnimationsController _animationsController;
        private float _moveSpeed;
        private bool _turnOnWay;
        
        private CancellationTokenSource _presenceTimeCancel;
        private CancellationTokenSource _handleOwnerChangeCancel;

        public BotAI(BotController bot, List<PathPoint> path, float moveSpeed)
        {
            _owner = bot;
            _path = path;
            _moveSpeed = moveSpeed;
            Init();
        }
        
        private void Init()
        {
            _transform = _owner.transform;
            _animationsController = _owner.GetComponent<AnimationsController>();

            var d = _owner.Radius * 2;
            _minCollisionDistSqr = d * d;
            _visitorLayerMask = 1 << AppConfig.Layers.Visitor.GetHashCode();
        }

        public void Start()
        {
            var firstPoint = _path.First();
            SetIdleState(firstPoint, firstPoint.Position);
        }

        public void Dispose()
        {
            _presenceTimeCancel?.Cancel();
            _handleOwnerChangeCancel?.Cancel();
        }

        public void HandleOwnerChange(PathPoint targetPoint, MovementStates state)
        {
            if (state == MovementStates.Idle)
            {
                _handleOwnerChangeCancel?.Cancel();
                State = state;
                CurrentTargetPoint = targetPoint;

                _handleOwnerChangeCancel = new CancellationTokenSource();
                UniTask.Delay(TimeSpan.FromSeconds(Random.Range(1f, 10f)), cancellationToken:_handleOwnerChangeCancel.Token)
                    .ContinueWith(() =>
                    {
                        SetNextTargetPoint(() => _owner.StartCoroutine(SetMovementState()));
                    });
            }
            else
            {
                CurrentTargetPoint = targetPoint;
                _owner.StartCoroutine(SetMovementState(false));
            }
        }

        private IEnumerator SetMovementState(bool needStartAnim = true)
        {
            State = MovementStates.Move;
            if(needStartAnim)
                _animationsController.PlayAnimation(AnimationsController.Animations.Walking);

            while (State == MovementStates.Move)
            {
                var pos = _transform.position;
                var dir = CurrentTargetPoint.Position - pos;
                var distSqr = dir.sqrMagnitude;
                
                if (distSqr > _minTargetDistSqr)
                {
                    if (distSqr < _minCollisionDistSqr && CurrentTargetPoint.PresenceTime > 0)
                    {
                        var ray = new Ray(pos + Vector3.up, dir.normalized * 2);
                        //Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 100);
                        if (Physics.Raycast(ray, 2, _visitorLayerMask))
                        {
                            _animationsController.PlayAnimation(AnimationsController.Animations.Idle);
                            SetIdleState(CurrentTargetPoint, pos);
                        }
                    }
                    
                    if (!_turnOnWay && Mathf.Abs(Vector3.Angle(dir, _transform.forward)) > 0.1f)
                    {
                        _turnOnWay = true;
                        _transform.DOLookAt(CurrentTargetPoint.Position, .4f)
                            .OnComplete(() => _turnOnWay = false);
                    }
                    
                    _transform.position = Vector3.MoveTowards(pos, CurrentTargetPoint.Position, _moveSpeed * Time.deltaTime);
                }
                else
                {
                    if (CurrentTargetPoint.PresenceTime > 0)
                    {
                        _animationsController.PlayAnimation(AnimationsController.Animations.Idle);
                        SetIdleState(CurrentTargetPoint, CurrentTargetPoint.Position);
                    }
                    else
                        SetNextTargetPoint();
                }
                yield return null;
            }
        }

        private void SetIdleState(PathPoint currentPoint, Vector3 pos)
        {
            _presenceTimeCancel?.Cancel();
            
            State = MovementStates.Idle;
            _transform.position = pos;
            _transform.DOLookAt(currentPoint.LookTarget, .6f, AxisConstraint.Y);
            
            _presenceTimeCancel = new CancellationTokenSource();
            UniTask.Delay(TimeSpan.FromSeconds(currentPoint.PresenceTime),
                    cancellationToken: _presenceTimeCancel.Token)
                .ContinueWith(() =>
                {
                    SetNextTargetPoint(() => _owner.StartCoroutine(SetMovementState()));
                });
        }
        
        private void SetNextTargetPoint(Action callback = null)
        {
            CurrentTargetPoint = _targetPointIndex >= _path.Count - 1 ? _path.First() : _path[_targetPointIndex + 1];
            _transform.DOLookAt(CurrentTargetPoint.Position, .4f, AxisConstraint.Y)
                .OnComplete(() => callback?.Invoke());
        }
    }
}