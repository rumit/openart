﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace OpenArtVirtualGallery.Gallery.Visitor.Bots
{
    public class EditorPathPoint : MonoBehaviour
    {
        [SerializeField] private Vector2 _presenceTimeRange;
        
        [SerializeField] private Transform _lookTarget;
        
        public float PresenceTime => Random.Range( _presenceTimeRange.x, _presenceTimeRange.y);
        public Vector3 Position => transform.position;

        public Vector3 GetLookTarget() => _lookTarget != null ? _lookTarget.position : Vector3.zero;
    }

    [Serializable]
    public class PathPoint
    {
        public float PresenceTime;
        public Vector3 Position;
        public Vector3 LookTarget;
        
        public override string ToString()
        {
            return $"[PresenceTime: {PresenceTime}, Position: {Position}, LookTarget: {LookTarget}]";
        }
    }
}