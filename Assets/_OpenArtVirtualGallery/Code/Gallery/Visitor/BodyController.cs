﻿using System;
using UnityEngine;
using Utils;

namespace OpenArtVirtualGallery.Visitor
{
    public class BodyController : MonoBehaviour
    {
        public AbstractVisitor Owner { get; private set; }
        
        public void Init(AbstractVisitor visitor)
        {
            Owner = visitor;
        }

        public void HideOwnerBody()
        {
            gameObject.SetLayerRecursively((int)AppConfig.Layers.PlayerBody);
        }
    }
}