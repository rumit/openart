﻿using System;
using System.Collections;
using OpenArtVirtualGallery.Visitor;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;

namespace OpenArtVirtualGallery.Gallery.UI
{
    [RequireComponent(typeof(RectTransform))]
    [ExecuteInEditMode]
    public class JoystickView : MonoBehaviour, IBeginDragHandler, IDragHandler, IPointerDownHandler, IPointerUpHandler, IJoystick
    {
        public event Action<float> PanAction;
        public event Action<Vector2> MoveAction;
        public event Action StopMoveAction;


        [MinMaxSlider(0, 1f)] 
        [SerializeField]
        private Vector2 _actionZones;
        
        [Space]
        [SerializeField] private GameObject _container;
        [SerializeField] private RectTransform _handle;
        [SerializeField] private Transform _point;
        [SerializeField] private UICircle _moveZoneShape;
        [SerializeField] private RectTransform _deadZoneTransform;
        [SerializeField] private RectTransform _rotateIcon;

        private RectTransform _rect;
        
        private bool _dragged;
        private bool _beginInDeadZone;
        private bool _inMovement;
        
        private float _radius;
        private Vector2 _deadZone;
        private Vector2 _panZone;
        private Vector2 _movementZone;
        private Vector2 _handlePosition;
        private readonly Vector2 _emptyVector = new (0, 0);

        private float _angle = 180;
        private Coroutine _pressedRoutine;

        private void Awake()
        {
            _rect = GetComponent<RectTransform>();
            _radius = _rect.rect.width * .5f;

            _deadZone = new Vector2(0, _actionZones.x * _radius);
            _panZone = new Vector2(_deadZone.y, _actionZones.y * _radius);
            _movementZone = new Vector2(_panZone.y, _radius);
        }
        
        public void Toggle(bool value) => _container.SetActive(value);

        public void OnPointerDown(PointerEventData eventData) { }

        public void OnBeginDrag(PointerEventData eventData)
        {
            transform.localEulerAngles = new Vector3(0, 0, _angle);
            _dragged = true;
            _beginInDeadZone = true;
            OnDrag(eventData);
            _pressedRoutine = StartCoroutine(OnPointerBeingPressed());
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!_dragged)
                return;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rect, eventData.position, eventData.pressEventCamera, out var pos);
            if (_beginInDeadZone == false)
            {
                var posDist = pos.magnitude;
                pos = posDist < _deadZone.y ? Vector2.LerpUnclamped(_emptyVector, pos, _deadZone.y / posDist) : pos;
                _handlePosition = Vector2.ClampMagnitude(pos, _radius);
            }
            else
            {
                _handlePosition = Vector2.ClampMagnitude(pos, _radius);
            }
            _handle.localPosition = _handlePosition;
            _point.localEulerAngles = new Vector3(0, 0, -transform.localEulerAngles.z);
            _rotateIcon.localEulerAngles = new Vector3(0, 0, -transform.localEulerAngles.z);
            
            //Debug.Log($"Zones dist {dist}, dead {_deadZone.y}, radius {_radius}, {_actionZones.x}");
            if (_handlePosition.magnitude < _deadZone.y && _beginInDeadZone)
                return;
            
            if (_beginInDeadZone)
               _beginInDeadZone = false;
            
            
            _angle = -Vector2.SignedAngle(Vector2.up, _handlePosition);
            PanAction?.Invoke(_angle);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _dragged = false;
            _beginInDeadZone = false;
            _handle.localPosition = _emptyVector;
            StopMoveAction?.Invoke();
            if(_pressedRoutine != null)
                StopCoroutine(_pressedRoutine);
        }

        private IEnumerator OnPointerBeingPressed()
        {
            while (_dragged)
            {
                var dist = _handlePosition.magnitude;
                if (dist > _movementZone.x)
                {
                    if (!_inMovement)
                        _inMovement = true;
                    MoveAction?.Invoke(_emptyVector);
                }
                else
                {
                    if (_inMovement)
                    {
                        _inMovement = false;
                        StopMoveAction?.Invoke();
                    }
                }
                
                yield return null;
            }
        }
        
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if(Application.isPlaying || _moveZoneShape == null || _deadZoneTransform == null)
                return;
            
            var radius = _rect.rect.width * .5f * GetComponentInParent<Canvas>().transform.localScale.x;
            _moveZoneShape.SetThickness((int)(radius - radius * _actionZones.y));
            _deadZoneTransform.sizeDelta = new Vector2(radius * _actionZones.x * 2, radius);
            //Debug.Log($"Zones deadZoneSqr {_deadZone}, panZoneSqr {_panZone}, movementZoneSqr: {_movementZone}, sqrRadius {_sqrRadius}");
        }
#endif
    }
}