﻿using OpenArtVirtualGallery.User;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.UI
{
    public class PlayerInputLookBlocker : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
    {
        [Inject] private UserActions _userActions;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            _userActions.ChatInteraction?.Invoke(true);
        }

        public void OnBeginDrag(PointerEventData eventData) { }

        public void OnEndDrag(PointerEventData eventData)
        {
            _userActions.ChatInteraction?.Invoke(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _userActions.ChatInteraction?.Invoke(false);
        }
    }
}