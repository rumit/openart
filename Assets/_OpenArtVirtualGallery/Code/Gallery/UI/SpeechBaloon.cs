﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery.Gallery.UI
{
    public class SpeechBaloon : MonoBehaviour
    {
        [Inject] private UserActions _userActions;
        [Inject] private LangSelector _langSelector;
        
        [SerializeField] private TextMeshProUGUI _textField;

        private GameObject _container;
        private CancellationTokenSource _cancelToken;
        private TweenerCore<Vector3, Vector3, VectorOptions> _tween;

        private void Awake()
        {
            _container = _textField.transform.parent.gameObject;
            _container.transform.localScale = Vector3.zero;
            _container.SetActive(false);

            _userActions.BotCollision += OnBotCollision;
        }

        private void OnDestroy()
        {
            _cancelToken?.Cancel();
            _userActions.BotCollision -= OnBotCollision;
        }

        private void OnBotCollision(BotController bot)
        {
            
            _cancelToken?.Cancel();
            _tween?.Kill();
            _container.transform.localScale = Vector3.zero;

            var speechData = bot.GetRandomSpeech();
            _textField.text = _langSelector.GetLocalText(speechData);
            
            _container.SetActive(true);
            _tween = _container.transform.DOScale(Vector3.one, .6f).SetEase(Ease.OutElastic);

            _cancelToken = new CancellationTokenSource();
            UniTask.Delay(TimeSpan.FromSeconds(4f), cancellationToken: _cancelToken.Token)
                .ContinueWith(() => _tween = _container.transform.DOScale(Vector3.zero, .2f)
                    .SetEase(Ease.InSine));
        }
    }
}