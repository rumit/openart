﻿using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.GalleryLevel
{
    public class EntryPoint : MonoBehaviour
    {
        public enum Locations
        {
            Top,
            Bottom
        }

        public Vector3 GetPosition() => transform.position;
        public Quaternion GetRotation() => transform.rotation;
    }
}