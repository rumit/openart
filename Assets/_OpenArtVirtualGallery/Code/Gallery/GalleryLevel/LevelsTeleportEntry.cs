﻿using System;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.GalleryLevel
{
    public class LevelsTeleportEntry : EntryPoint
    {
        [SerializeField] private Locations _location;
        public Locations Location => _location;
        
        private LevelsTeleportExitPoint _exitPoint;

        private void Start()
        {
            _exitPoint = GetComponentInChildren<LevelsTeleportExitPoint>();
        }

        public Vector3 GetExitPoint()
        {
            var randX = .1f + UnityEngine.Random.value * .1f;
            var randY = .1f + UnityEngine.Random.value * .1f;
            return _exitPoint.transform.position + new Vector3(randX, 0, randY);
        }
    }
}