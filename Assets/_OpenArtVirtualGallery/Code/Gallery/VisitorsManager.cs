﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery
{
    public class VisitorsManager : ILateTickable
    {
        public event Action<AbstractVisitor> VisitorAdded;
        public event Action<AbstractVisitor> VisitorRemoved;
        
        //[Inject] private DebugLog _log;
        
        public readonly List<AbstractVisitor> List = new List<AbstractVisitor>();
        private VisitorController _localPlayerVisitor;
        
        public void Add(AbstractVisitor hero)
        {
            if (hero is VisitorController h && h.PhotonView.IsMine)
                _localPlayerVisitor = h;

            if (List.Contains(hero)) 
                return;
            
            List.Add(hero);
            VisitorAdded?.Invoke(hero);
        }

        public void Remove(AbstractVisitor hero)
        {
            if (!List.Contains(hero)) 
                return;
            
            List.Remove(hero);
            VisitorRemoved?.Invoke(hero);
        }

        public VisitorController GetLocalPlayerHero() => _localPlayerVisitor;
        public async UniTask<VisitorController> GetLocalPlayerHeroAsync()
        {
           await UniTask.WaitUntil(() => _localPlayerVisitor != null);
           return _localPlayerVisitor;
        }

        public AbstractVisitor GetHeroByUserID(string userID)
        {
            return List.FirstOrDefault(h => h.Model.Name == userID);
        }
        
        public void LateTick()
        {
            foreach (var hero in List)
            {
                switch (hero)
                {
                    case VisitorController h when !h.PhotonView.IsMine:
                        h.UpdateNameDisplay(_localPlayerVisitor.CinemachineTarget);
                        break;
                    //case BotController bot:
                        //bot.BotAI?.Update();
                        //break;
                }
            }
        }
    }
}