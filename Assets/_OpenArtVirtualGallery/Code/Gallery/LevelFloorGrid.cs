﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Grid
{
    public class LevelFloorGrid : MonoBehaviour
    {
        [SerializeField] 
        [Range(1, 2)] private float _celSize = 1;
        [SerializeField] private Vector3 _size;
        [SerializeField] private GameObject _cellAnchorRef;
        
        //private GameObject _cellAnchor;
        
        private List<GridCell> _cells = new List<GridCell>(5000);
        //private Hashtable _occupiedCellsTable;

        public void Init()
        {
            //_occupiedCellsTable = new Hashtable();
                //PhotonNetwork.CurrentRoom.CustomProperties;

            var rootPos = transform.position;
            var halfWidth = _size.x * .5f;
            var halfHeight = _size.z * .5f;
            var ltp = new Vector3(-halfWidth, _size.y, halfHeight);
            var rect = new Rect(ltp.x, ltp.z, _size.x, _size.z);

            var numCols = Mathf.Ceil(rect.width / _celSize);
            var numRows = Mathf.Ceil(rect.height / _celSize);
            for (var i = 0; i < numCols; i++)
            {
                for (var j = 0; j < numRows; j++)
                {
                    var pos = new Vector3(i - halfWidth + rootPos.x, _size.y, j - halfHeight + rootPos.z);
                    var cell = new GridCell(new Bounds(pos, new Vector3(_celSize, 0, _celSize)));
                    _cells.Add(cell);
                }
            }
        }

        public void SetCellOccupied(Vector3 position)
        {
            var pos = new Vector3(position.x, _size.y, position.z);
            foreach (var cell in _cells.Where(cell => cell.Rect.Contains(pos)))
            {
                //_roomPropsTable.Add(cell.Key, true);
                AddOccupiedCell(cell.Key, true);
                Debug.Log($"CustomProperties: {SupportClass.DictionaryToString(PhotonNetwork.CurrentRoom.CustomProperties)}");
                //Debug.Log($"cell occupied {cell} ->>> {_roomPropsTable[cell.Key]}");
                //cell.Occupied = true;
                //_cellAnchor = Instantiate(_cellAnchorRef, new Vector3(cell.Position.x, _size.y + 0.01f, cell.Position.z),
                //Quaternion.Euler(90, 0, 0));
                return;
            }
        }

        public void SetCellFree(Vector3 position)
        {
            var pos = new Vector3(position.x, _size.y, position.z);
            foreach (var cell in _cells.Where(cell => cell.Rect.Contains(pos)))
            {
                //_roomPropsTable.Remove(cell.Key);
                RemoveOccupiedCell(cell.Key);
                //cell.Occupied = false;
                //if(_cellAnchor != null)
                //Destroy(_cellAnchor);
                return;
            }
        }

        public bool IsPositionOccupied(Vector3 position)
        {
            var pos = new Vector3(position.x, _size.y, position.z);
            foreach (var cell in _cells)
            {
                if (cell.Rect.Contains(pos))
                {
                    //Debug.Log($"IsPositionOccupied cell: {cell}, [{pos.x},{pos.y},{pos.z}], table count: {_occupiedCellsTable.Count}");
                    //foreach (var kvp in _roomPropsTable)
                    //{
                    //    Debug.Log($"key: {kvp.Key}, value: {kvp.Value}");
                   // }
                    if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(cell.Key, out var isOccupied) && (bool)isOccupied)
                        return true;
                }
            }
            return false;
        }

        public Vector3 GetNearestFreePosition(Vector3 occupiedPos)
        {
            var pos = new Vector3(occupiedPos.x, _size.y, occupiedPos.z);
            var min = float.MaxValue;
            var result = pos;
            foreach (var cell in _cells)
            {
                var dist = (cell.Position - pos).sqrMagnitude;
                if (dist < min)
                {
                    min = dist;
                    result = cell.Position;
                }
            }
            return result;
        }
        
        private void AddOccupiedCell(string key, bool value)
        {
            //var t = PhotonNetwork.CurrentRoom.CustomProperties;
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable{{key, value}});
        }
        
        private void RemoveOccupiedCell(string key)
        {
            var t = PhotonNetwork.CurrentRoom.CustomProperties;
            t.Remove(key);
            PhotonNetwork.CurrentRoom.SetCustomProperties(t);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(0.29f, 0.43f, 0.58f);

            var rootPos = transform.position;
            var halfWidth = _size.x * .5f;
            var halfHeight = _size.z * .5f;
            var ltp = new Vector3(-halfWidth, _size.y, halfHeight);
            var rect = new Rect(ltp.x, ltp.z, _size.x, _size.z);

            var numCols = Mathf.Ceil(rect.width / _celSize);
            var numRows = Mathf.Ceil(rect.height / _celSize);
            for (var i = 0; i < numCols; i++)
            {
                for (var j = 0; j < numRows; j++)
                {
                    var pos = new Vector3(i - halfWidth + rootPos.x, _size.y, j - halfHeight + rootPos.z);
                    var cellRect = new Bounds(pos, new Vector3(_celSize, 0, _celSize));
                    Gizmos.DrawWireCube(cellRect.center, cellRect.size);
                }
            }
        }
    }

    public class GridCell
    {
        public Bounds Rect { get; }
        public Vector3 Position { get; }
        public string Key => $"{Position.x:0.##}_{Position.y:0.##}_{Position.z:0.##}";
        //public bool Occupied;

        public GridCell(Bounds rect)
        {
            Rect = rect;
            Position = rect.center;
        }

        public override string ToString()
        {
            return $"Cell: [{Key}]";
        }
    }
}