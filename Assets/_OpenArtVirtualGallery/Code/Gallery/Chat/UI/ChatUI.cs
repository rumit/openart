﻿using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using OpenArtVirtualGallery.Visitor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static LangBaseValuesConfig;

namespace OpenArtVirtualGallery.Gallery.Chat.UI
{
    public class ChatUI : MonoBehaviour
    {
        [SerializeField] private GameObject _view;
        [SerializeField] private GameObject _chatWindow;
        [SerializeField] private Button _toggleChatBtn;
        [SerializeField] private Button _telegramBtn;

        [Space(10)]
        [SerializeField] private VisitorChatItem _visitorChatItemRef;
        
        [SerializeField] private Transform _visitorItemsContainer;

        [Space(10)]
        [SerializeField] private ScrollRect _messagesScrollRect;

        [SerializeField] private TextMeshProUGUI _messagesField;
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private Button _sendBtn;

        private AppConfig.URLs _urls;
        private VisitorsManager _visitorsManager;
        private ChatManager _chatManager;
        private LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;

        private readonly List<VisitorChatItem> _visitorItems = new List<VisitorChatItem>();

        private TextMeshProUGUI _toggleChatBtnTextField;
        private bool _opened;
        private VisitorChatItem _selectedVisitorItem;

        [Inject]
        private void Inject(AppConfig.URLs urls, 
            ChatManager chatManager,
            VisitorsManager visitorsManager,
            LangValuesConfig<BaseLangIds>.LangValues langValues)
        {
            _urls = urls;
            _chatManager = chatManager;
            _visitorsManager = visitorsManager;
            _langBaseValues = langValues;
        }

        private void OnDestroy()
        {
            _toggleChatBtn.onClick.RemoveListener(OnChatWindowToggle);
            _sendBtn.onClick.RemoveListener(OnSendBtnClick);
            _telegramBtn.onClick.RemoveListener(OnTelegramBtnClick);

            _visitorsManager.VisitorAdded -= OnVisitorAdded;
            _visitorsManager.VisitorRemoved -= OnVisitorRemoved;
            
            _chatManager.MessageReceived -= OnMessageReceived;
            _chatManager.ChatDisconnected -= OnChatDisconnected;
            _chatManager.ChatConnected -= OnChatConnected;
        }

        private void Awake()
        {
            Debug.Log($"chat > Awake, id view active: {_chatManager}");
            _view.SetActive(false);
            
            _toggleChatBtnTextField = _toggleChatBtn.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
            _sendBtn.GetComponentInChildren<TextMeshProUGUI>().text = _langBaseValues.Get(BaseLangIds.Send);
            _toggleChatBtn.onClick.AddListener(OnChatWindowToggle);
            _sendBtn.onClick.AddListener(OnSendBtnClick);
            _telegramBtn.onClick.AddListener(OnTelegramBtnClick);
            
            _visitorsManager.VisitorAdded += OnVisitorAdded;
            _visitorsManager.VisitorRemoved += OnVisitorRemoved;
            _chatManager.ChatConnected += OnChatConnected;
            _chatManager.ChatDisconnected += OnChatDisconnected;

        }

        private void SetupView()
        {
            _view.SetActive(true);
            UpdateChatWindowState();
        }

        private void OnChatConnected()
        {
            _chatManager.MessageReceived += OnMessageReceived;
            
            Debug.Log($"OnChatConnected");

            if (!_view.activeSelf)
                SetupView();
            _messagesField.text = "";
        }

        private void OnChatDisconnected()
        {
            _view.SetActive(false);
            _chatManager.MessageReceived -= OnMessageReceived;
            //_chatManager.ChatDisconnected -= OnChatDisconnected;
            // _messagesField.text = $"<color=#{ColorUtility.ToHtmlStringRGB(Color.red)}>Chat disconnected!<br>Trying to reconnect...</color>\n";
        }
        
        private async void OnMessageReceived(string message)
        {
            _messagesField.text += $"{message}";
            await UniTask.DelayFrame(3);
            _messagesScrollRect.normalizedPosition = new Vector2(0, 0);
        }

        private void OnSendBtnClick()
        {
            if(string.IsNullOrWhiteSpace(_inputField.text))
                return;
            
            if(_selectedVisitorItem == null)
                _chatManager.PublishMessage(_inputField.text);
            else
                _chatManager.SentPrivateMessage(_selectedVisitorItem.Id, _inputField.text);
            
            _inputField.text = "";
        }

        private void OnChatWindowToggle()
        {
            _opened = !_opened;
            UpdateChatWindowState();
        }

        private void UpdateChatWindowState()
        {
            _chatWindow.SetActive(_opened);
            _toggleChatBtnTextField.text = _opened ? _langBaseValues.Get(BaseLangIds.CloseChat) : _langBaseValues.Get(BaseLangIds.OpenChat);
        }

        private void OnVisitorAdded(AbstractVisitor visitor)
        {
            if(_visitorsManager.GetLocalPlayerHero() == visitor || visitor is BotController)
                return;

            var item = Instantiate(_visitorChatItemRef, _visitorItemsContainer);
            item.Init(visitor.Config.Color, visitor.Model.Name, OnVisitorItemSelectRequest);
            _visitorItems.Add(item);
        }

        private void OnVisitorItemSelectRequest(VisitorChatItem item)
        {
            if (_selectedVisitorItem == item)
            {
                _selectedVisitorItem.Deselect();
                _selectedVisitorItem = null;
                return;
            }

            if(_selectedVisitorItem != null)
                _selectedVisitorItem.Deselect();
            item.Select();
            _selectedVisitorItem = item;
        }

        private void OnVisitorRemoved(AbstractVisitor visitor)
        {
            if (visitor is BotController)
                return;
            
            var item = _visitorItems.FirstOrDefault(i => i.Id == visitor.Model.Name);
            if (item == null) 
                return;
            _visitorItems.Remove(item);
            Destroy(item.gameObject);
        }

        private void OnTelegramBtnClick()
        {
            Application.OpenURL(_urls.TelegramChatUrl);
        }
    }
}