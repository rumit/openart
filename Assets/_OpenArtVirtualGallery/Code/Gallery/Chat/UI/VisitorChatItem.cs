﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace OpenArtVirtualGallery.Gallery.Chat.UI
{
    public class VisitorChatItem : MonoBehaviour
    {
        private event Action<VisitorChatItem> _itemClick;
        
        [SerializeField] private TextMeshProUGUI _textField;
        
        private Image _bground;
        private Button _button;
        
        private Color _defaultBgroundColor;
        private Color _defaultItemColor;

        public bool IsSelected { get; private set; }
        public string Id { get; private set; }

        private void OnDestroy()
        {
            if(_button)
                _button.onClick.RemoveListener(OnItemClick);
        }

        private void Start()
        {
            _button = GetComponent<Button>();
            _bground = GetComponent<Image>();

            _defaultBgroundColor = _bground.color;

            _button.onClick.AddListener(OnItemClick);
        }
        
        public void Init(Color color, string useriD, Action<VisitorChatItem> selectAction)
        {
            Id = useriD;
            _textField.color = color;
            _textField.text = useriD;
            _defaultItemColor = color;
            _itemClick = selectAction;
        }

        public void Select()
        {
            IsSelected = true;
            _textField.color = Color.white;
            _bground.color = _defaultItemColor;
        }
        
        public void Deselect()
        {
            IsSelected = false;
            _textField.color = _defaultItemColor;
            _bground.color = _defaultBgroundColor;
        }
        
        private void OnItemClick()
        {
            _itemClick?.Invoke(this);
        }
    }
}