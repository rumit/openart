﻿using System;
using OpenArtVirtualGallery.User;
using TMPro;
using UnityEngine;
using Zenject;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery.Gallery.Chat.UI
{
    public class ChatInputField : MonoBehaviour
    {
        [Inject] private UserActions _userActions;
        
        private TMP_InputField _input;

        private void OnDestroy()
        {
            if(_input == null)
                return;
            _input.onSelect.RemoveListener(OnFieldSelected);
            _input.onDeselect.RemoveListener(OnFieldDeselected);
        }

        private void Start()
        {
            _input = GetComponent<TMP_InputField>();
            _input.onSelect.AddListener(OnFieldSelected);
            _input.onDeselect.AddListener(OnFieldDeselected);
        }

        private void OnFieldSelected(string text)
        {
            _userActions.ChatInputFieldTyping.Invoke(true);
        }
        
        private void OnFieldDeselected(string text)
        {
            _userActions.ChatInputFieldTyping.Invoke(false);
        }
    }
}