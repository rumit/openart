﻿using System;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon;
using OpenArtVirtualGallery.User;
using UnityEngine;
using Photon.Chat;
using Photon.Pun;
using Zenject;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery.Gallery.Chat
{
    public class ChatManager : MonoBehaviour, IChatClientListener
    {
        private const string userColor = "#888888";
        private const string galleryChat = "galleryChat";
        
        public event Action<string> MessageReceived;
        public event Action ChatDisconnected;
        public event Action ChatConnected;

        private VisitorsManager _visitorsManager;
        private LocalUserHeroResolver _localUserHeroResolver;
        private ChatClient _chatClient;
        private string _userID;

        [Inject]
        private void Inject(VisitorsManager visitorsManager, LocalUserHeroResolver localUserHeroResolver)
        {
            _visitorsManager = visitorsManager;
            _localUserHeroResolver = localUserHeroResolver;
        }
        
        public void OnDestroy() => DisconnectInternal();

        public void Initialize()
        {
            var localUser = _localUserHeroResolver.Get();
            _userID = localUser.Name;
            
            _chatClient = new ChatClient(this);
            _chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion,
                new AuthenticationValues(_userID));
        }
        
        public void OnApplicationQuit() => DisconnectInternal();

        public void Update() => _chatClient?.Service();

        public void Disconnect() => DisconnectInternal();

        private void DisconnectInternal()
        {
            _chatClient?.Disconnect();
        }

        public void PublishMessage(string message)
        {
            Debug.Log($"OnPublishMessage {message}");
            _chatClient.PublishMessage(galleryChat, message);
        }
        
        public void SentPrivateMessage(string userID, string message)
        {
            _chatClient.SendPrivateMessage(userID, message);
        }

        public void DebugReturn(DebugLevel level, string message)
        {
            Logger.Log($"{level}, {message}");
        }

        public void OnDisconnected()
        {
            _chatClient.Unsubscribe(new []{galleryChat});
        }

        public void OnConnected()
        {
            Logger.Log($"chat > OnConnected");
            ChatConnected?.Invoke();
            _chatClient.Subscribe(galleryChat);
        }

        public void OnChatStateChange(ChatState state)
        {
            Logger.Log($"chat > OnChatStateChange, state: {state}");
            if (state == ChatState.Disconnected)
            {
                ChatDisconnected?.Invoke();
                // await UniTask.DelayFrame(1000);
                //_chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion,
                  //  new AuthenticationValues(_userID));
            }
        }

        public void OnGetMessages(string channelName, string[] senders, object[] messages)
        {
            var result = "";
            
            for (var i = 0; i < senders.Length; i++)
            {
                var sender = senders[i];
                var senderVisitor = _visitorsManager.GetHeroByUserID(sender);
                if(senderVisitor == null)
                    continue;
                var color = _userID == sender ? userColor : $"#{ColorUtility.ToHtmlStringRGB(senderVisitor.Config.Color)}";
                result += $"<color={color}>{sender}</color>: {messages[i]}\n";
            }

            Debug.Log($"OnGetMessages > result: {result}");
            MessageReceived?.Invoke(result);
        }

        public void OnPrivateMessage(string sender, object message, string channelName)
        {
            var senderHero = _visitorsManager.GetHeroByUserID(sender);
            if(senderHero == null)
                return;
            var color = _userID == sender ? userColor : $"#{ColorUtility.ToHtmlStringRGB(senderHero.Config.Color)}";
            var mess = $"<color=#ff0000>Приватное сообщение от</color> <color={color}>{sender}</color>: {message}\n"; 
            
            MessageReceived?.Invoke(mess);
            Logger.Log($"{mess}");
        }

        public void OnSubscribed(string[] channels, bool[] results)
        {
            for (var i = 0; i < channels.Length; i++)
                Logger.Log($"Вы подключены к {channels[i]}, {results[i]}");
        }

        public void OnUnsubscribed(string[] channels) { }

        public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
        {
            Logger.Log($"[user: {user}, status: {status}, got message: {gotMessage}, message: {message}]");
        }

        public void OnUserSubscribed(string channel, string user)
        {
            Logger.Log($"Пользователь {user} подключился к {channel}");
        }

        public void OnUserUnsubscribed(string channel, string user) { }
    }
}