﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Configs
{
    public class DignityFestConfig : SerializedScriptableObject
    {
        [SerializeField] private ExhibitorConfig.AboutInfo _info;

        public ExhibitorConfig.AboutInfo Info => _info;
    }
}