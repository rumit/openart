﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Configs
{
    public class BotSpeechesConfig : SerializedScriptableObject
    {
        public readonly LangValueData[] Speeches;
        public LangValueData GetRandomSpeech() => Speeches[Random.Range(0, Speeches.Length)];
    }
}