﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace OpenArtVirtualGallery.Gallery.Configs
{
    public class ExhibitorConfig : SerializedScriptableObject
    {
        public readonly LangValueData Name;
        public readonly AboutInfo About;
        public readonly LinkData[] Links;
        public readonly MediaData Media;

        [Serializable]
        public class AboutInfo : ILangVaueData
        {
            [SerializeField][TextArea(10, 500)] private string _eng;
            [SerializeField][TextArea(10, 500)] private string _ru;

            public string Eng => _eng;
            public string Ru => _ru;
        }
        
        public readonly struct MediaData
        {
            public readonly AudioClip[] Audio;
            public readonly string[] Videos;
            public readonly ArtVideoData[] ArtVideos;
        }
        
        public readonly struct ArtVideoData
        {
            public readonly LangValueData Info;
            public readonly string Url;
            public readonly bool IsSplashScreen;
        }
        
        public readonly struct LinkData
        {
            public readonly string CommentBefore;
            public readonly string URL;
        }
    }
}