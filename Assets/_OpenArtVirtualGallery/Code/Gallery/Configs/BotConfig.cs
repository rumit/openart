﻿using Sirenix.OdinInspector;

namespace OpenArtVirtualGallery.Gallery.Configs
{
    public class BotConfig : SerializedScriptableObject
    {
        public readonly BotSpeechesConfig FirstFloorSpeechesConfig;
        public readonly BotSpeechesConfig SecondFloorSpeechesConfig;
        public readonly float MoveSpeed = 1.8f;
    }
}