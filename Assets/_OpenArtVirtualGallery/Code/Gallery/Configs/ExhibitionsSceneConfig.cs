﻿using System;
using Cinemachine;
using OpenArtVirtualGallery.Gallery.GalleryLevel;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;
using UnityEngine;

namespace OpenArtVirtualGallery.Gallery.Configs
{
    [Serializable]
    public class ExhibitionsSceneConfig
    {
        public enum GalleryFloors
        {
            First,
            Second
        }
        
        [SerializeField] private Camera _camera;
        public Camera Camera => _camera;
        
        [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
        public CinemachineVirtualCamera CinemachineVirtualCamera => _cinemachineVirtualCamera;
        
        [SerializeField] private float _maxDistance = 30;
        public float MaxDistance => _maxDistance;
        
        [SerializeField] private EntryPoint _entryPoint;
        public EntryPoint EntryPoint => _entryPoint;
        
        [SerializeField] private BotSpawnArea[] _spawnBotsAreas;
        public BotSpawnArea[] SpawnBotsAreas => _spawnBotsAreas;

        [Header("[--- UI ---]")]
        [SerializeField] private Transform _uiRoot;
        public Transform UIRoot => _uiRoot;
        
        [SerializeField] private GameObject _botSpeechBaloonUIRef;
        public GameObject BotSpeechBaloonUIRef => _botSpeechBaloonUIRef;
        
        [SerializeField] private GameObject _exhibitDisplaysUIRef;
        public GameObject ExhibitDisplaysUIRef => _exhibitDisplaysUIRef;
#if UNITY_ANDROID || UNITY_IOS
        [SerializeField] private JoystickView _joystickRef;
        public JoystickView JoystickRef => _joystickRef;
#endif
    }
}