﻿using OpenArtVirtualGallery.Gallery.Chat;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Zenject;

namespace OpenArtVirtualGallery
{
    public class JoinRoomController : MonoBehaviourPunCallbacks
    {
        private ScenesManager _scenesManager;
        private ChatManager _chatManager;
        private AppConfig.PublishSettings _publishSettings;
        private string _sceneToLoad;

        [Inject]
        private void Inject(ScenesManager scenesManager, 
            ChatManager chatManager,
            AppConfig.PublishSettings publishSettings)
        {
            _scenesManager = scenesManager;
            _chatManager = chatManager;
            _publishSettings = publishSettings;
        }
        
        private void Start()
        {
            PhotonNetwork.GameVersion = "1.0.0";
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.UseAddressablesSceneLoading = true;
        }
        
        public void JoinRoom(string roomId)
        {
            _scenesManager.PrepareRemoteSceneLoading();
            
            _sceneToLoad = roomId;

            if (PhotonNetwork.CurrentRoom != null)
            {
                PhotonNetwork.LeaveRoom();
                _chatManager.Disconnect();
                return;
            }
            
            var options = new RoomOptions
            {
                MaxPlayers = 20,
                PublishUserId = true,
                IsOpen = true
            };

            var postfix = _publishSettings.IsDebug ? "_Debug" : "_Release";
            var roomName = $"Room_{_sceneToLoad}_[D6A46CD7-0A0A-4141-B7B9-AD69BEAEB590]_{postfix}";
            Debug.Log($"{PhotonNetwork.NickName} try connect to Room {roomName}");
            PhotonNetwork.JoinOrCreateRoom(roomName, options, null);
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            var options = new RoomOptions
            {
                MaxPlayers = 20,
                PublishUserId = true,
                IsOpen = true
            };

            var postfix = _publishSettings.IsDebug ? "_Debug" : "_Release";
            var roomName = $"Room_{_sceneToLoad}_[D6A46CD7-0A0A-4141-B7B9-AD69BEAEB590]_{postfix}";
            Debug.Log($"{PhotonNetwork.NickName} try connect to Room {roomName}");
            PhotonNetwork.JoinOrCreateRoom(roomName, options, null);
        }

        public override void OnJoinedRoom()
        {
            if(!PhotonNetwork.IsMasterClient || _sceneToLoad == null)
                return;

            base.OnJoinedRoom();
            Debug.Log($"{PhotonNetwork.NickName} successfully connected to Room {PhotonNetwork.CurrentRoom.Name}");
            _scenesManager.LoadRemoteScene(_sceneToLoad);
            _sceneToLoad = null;
        }
    }
}