﻿using System;
using OpenArtVirtualGallery.Visitor;
using UnityEngine;
using UnityEngine.Serialization;

namespace OpenArtVirtualGallery.User
{
    public class LocalUserHeroResolver
    {
        private VisitorHeroModel _locUserHero;
        public LocalUserHeroResolver(VisitorHeroModel data)
        {
            _locUserHero = data;
            
        }
        public VisitorHeroModel Get() => _locUserHero;
    }
    
    [Serializable]
    public class VisitorHeroModel
    {
        public string Name;
        public int Index;
        public string PhotonUserId;
        public bool LegalAgeConfirmed;
        
        public readonly bool IsLocal;
        public PlayerMotionController PlayerMotionController;

        public VisitorHeroModel(bool isLocal)
        {
            IsLocal = isLocal;
        }

        public override string ToString()
        {
            return $"[Name: {Name}, Index: {Index}, PhotonActorNumber: {PhotonUserId}]";
        }
    }
}
    
