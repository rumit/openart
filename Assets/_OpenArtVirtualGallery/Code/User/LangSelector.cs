﻿using System;
using OpenArtVirtualGallery.UI.Items;
using Zenject;

namespace OpenArtVirtualGallery.User
{
    public class LangSelector : IInitializable, IDisposable
    {
        public enum Langs
        {
            En,
            Ru
        }

        [Inject] private UserActions _userActions;

        public Langs Current { get; private set; }

        public string GetLocalText(ILangVaueData descriptionData)
        {
            return Current == Langs.En ? descriptionData.Eng : descriptionData.Ru;
        }
        
        public void Initialize()
        {
            _userActions.LangSelected += OnSelected;
        }
        
        public void Dispose()
        {
            _userActions.LangSelected -= OnSelected;
        }

        private void OnSelected(LangData data)
        {
            Current = data.Lang;
        }
    }
    
    [Serializable]
    public class LangData : IItemDataProvider
    {
        public LangSelector.Langs Lang;
    }
}