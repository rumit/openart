﻿using System;
using OpenArtVirtualGallery.Gallery.GalleryLevel;
using OpenArtVirtualGallery.Gallery.Visitor.Bots;

namespace OpenArtVirtualGallery.User
{
    public class UserActions
    {
        public Action<LangData> LangSelected;
        
        public Action<LevelsTeleportEntry> EntryTeleportArea;
        public Action LeaveTeleportArea;
        
        public Action<bool> ChatInteraction;
        public Action<bool> ChatInputFieldTyping;
        
        public Action<bool> TogglePlayerNavigation;
        public Action<BotController> BotCollision;
    }
}