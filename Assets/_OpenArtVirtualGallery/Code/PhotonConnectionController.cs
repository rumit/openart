﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Zenject;
using static LangBaseValuesConfig;
using Logger = OpenArtVirtualGallery.Utils.Logger;

namespace OpenArtVirtualGallery
{
    public class PhotonConnectionController : MonoBehaviourPunCallbacks
    {
        [SerializeField] private Transform _popupContainer;

        private ScenesManager _scenesManager;
        private AppConfig.CommonAppPrefabs _commonAppPrefabs;
        private LangValuesConfig<BaseLangIds>.LangValues _langBaseValues;

        [Inject]
        private void Inject(ScenesManager scenesManager, AppConfig.CommonAppPrefabs commonAppPrefabs, 
            LangValuesConfig<BaseLangIds>.LangValues langBaseValues)
        {
            _scenesManager = scenesManager;
            _commonAppPrefabs = commonAppPrefabs;
            _langBaseValues = langBaseValues;
        }
        
        public override void OnDisconnected(DisconnectCause cause)
        {
            var mess = _langBaseValues.Get(BaseLangIds.ServerDisconnect);
            ModalPopupsManager.ShowPopup(_commonAppPrefabs.GetNotificationPopupPrefab(), _popupContainer, mess);
            ModalPopupsManager.Current.ActionSelected += OnPopupClosed;
        }

        private void OnPopupClosed(bool value, IPopup popup)
        {
            ModalPopupsManager.Current.ActionSelected -= OnPopupClosed;
            ModalPopupsManager.RemovePopup();
            _scenesManager.LoadLocalScene(ScenesManager.Scenes.AuthorScene.ToString());
        }
    }
}