﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace OpenArtVirtualGallery.CommonComponents
{
    public class TransformRotator : MonoBehaviour
    {
        private TweenerCore<Quaternion, Vector3, QuaternionOptions> _tween;

        private void OnEnable()
        {
            _tween = transform.DOLocalRotate(new Vector3(0, 0, 360), 2f, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetEase(Ease.Linear);
        }

        private void OnDisable()
        {
            _tween.Kill();
            transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }
}