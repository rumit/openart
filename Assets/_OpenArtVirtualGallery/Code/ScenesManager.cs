using System;
using Cysharp.Threading.Tasks;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace OpenArtVirtualGallery
{
    public class ScenesManager
    {
        public enum Scenes
        {
            BootstrapScene,
            AuthorScene,
            OpenArt_2021,
            OpenArt_2022,
            OpenArt_2023_2024
        }

        public event Action SceneLoadStart;
        public event Action<float> SceneLoadProgress;
        public event Action SceneLoaded;

        public Scene Current { get; private set; }

        public ScenesManager()
        {
            LoadLocalScene(Scenes.AuthorScene.ToString());
            PhotonNetwork.InitLoadAddressableSceneCallback(OnInitLoadAddressableSceneCallback);
        }

        private void OnInitLoadAddressableSceneCallback(string scene)
        {
            Debug.Log($"OnInitLoadAddressableSceneCallback {scene}");
            LoadRemoteScene(scene);
        }

        public void PrepareRemoteSceneLoading()
        {
            SceneLoadStart?.Invoke();
        }

        public async void LoadLocalScene(string scene)
        {
            SceneManager.LoadSceneAsync(scene);
        }
        
        public async void LoadRemoteScene(string scene)
        {
            var operation = Addressables.LoadSceneAsync(scene, LoadSceneMode.Single, false);

            await UniTask.WaitUntil(() =>
            {
                var status = operation.GetDownloadStatus();
                SceneLoadProgress?.Invoke(status.Percent);
                return operation.IsDone;
            });
            
            Current = operation.Task.Result.Scene;
            await operation.Task.Result.ActivateAsync();
            
            SceneLoaded?.Invoke();
        }
    }
}