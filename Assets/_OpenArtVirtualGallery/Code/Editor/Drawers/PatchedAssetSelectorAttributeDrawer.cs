﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector.Editor.Drawers;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

[DrawerPriority(0.0, 0.0, 2003.0)]
public sealed class PatchedAssetSelectorAttributeDrawer :
    OdinAttributeDrawer<CustomAssetSelectorAttribute>
{
    private GUIContent label;
    private bool isList;
    private bool isListElement;
    private Func<IEnumerable<ValueDropdownItem>> getValues;
    private Func<IEnumerable<object>> getSelection;
    private Type elementOrBaseType;
    private bool isString;
    private IEnumerable<object> result;
    private bool enableMultiSelect;

    /// <summary>Initializes this instance.</summary>
    protected override void Initialize()
    {
        isList = Property.ChildResolver is IOrderedCollectionResolver;
        isListElement = Property.Parent != null && Property.Parent.ChildResolver is IOrderedCollectionResolver;
        getSelection = () => Property.ValueEntry.WeakValues.Cast<object>();
        elementOrBaseType = isList ? (Property.ChildResolver as IOrderedCollectionResolver).ElementType : Property.ValueEntry.BaseValueType;
        isString = elementOrBaseType == typeof(string);
        getValues = () =>
        {
            string filter = Attribute.Filter ?? "";
            if (string.IsNullOrEmpty(filter) && !typeof(Component).IsAssignableFrom(elementOrBaseType) && !elementOrBaseType.IsInterface)
                filter = "t:" + elementOrBaseType.Name;
            return AssetDatabase.FindAssets(filter, Attribute.SearchInFolders ?? new string[0])
                .Select(x => AssetDatabase.GUIDToAssetPath(x)).Distinct().SelectMany(x =>
                    (!x.EndsWith(".unity", StringComparison.InvariantCultureIgnoreCase)
                        ? AssetDatabase.LoadAllAssetsAtPath(x)
                        : Enumerable.Repeat(AssetDatabase.LoadAssetAtPath(x, typeof(Object)), 1))
                    .Where(obj =>
                        obj != (Object) null && elementOrBaseType.IsAssignableFrom(obj.GetType())).Select(obj => new
                    {
                        o = obj,
                        p = x
                    })).Select(x => new ValueDropdownItem
                {
                    Text = Attribute.ShortNames
                        ? ObjectNames.NicifyVariableName(x.o.name)
                        : NicifyAssetPath(x.p) + (AssetDatabase.IsMainAsset(x.o)
                            ? ""
                            : "/" + x.o.name),
                    Value = isString ? (object) x.p : (object) x.o
                });
        };
    }
    
    public static string NicifyAssetPath(string path)
    {
        return Path.GetDirectoryName(path)?.Replace("\\", "/")
               + "/" + ObjectNames.NicifyVariableName(Path.GetFileNameWithoutExtension(path));
    }

    private static IEnumerable<ValueDropdownItem> ToValueDropdowns(
        IEnumerable<object> query)
    {
        return query.Select(x =>
        {
            switch (x)
            {
                case ValueDropdownItem valueDropdownItem2:
                    return valueDropdownItem2;
                case IValueDropdownItem _:
                    IValueDropdownItem valueDropdownItem1 = x as IValueDropdownItem;
                    return new ValueDropdownItem(valueDropdownItem1.GetText(), valueDropdownItem1.GetValue());
                default:
                    return new ValueDropdownItem(null, x);
            }
        });
    }

    /// <summary>
    /// Draws the property with GUILayout support. This method is called by DrawPropertyImplementation if the GUICallType is set to GUILayout, which is the default.
    /// </summary>
    protected override void DrawPropertyLayout(GUIContent label)
    {
        this.label = label;
        if (Property.ValueEntry == null)
            CallNextDrawer(label);
        else if (isList)
        {
            if (Attribute.DisableListAddButtonBehaviour)
            {
                CallNextDrawer(label);
            }
            else
            {
                var customAddFunction = CollectionDrawerStaticInfo.NextCustomAddFunction;
                CollectionDrawerStaticInfo.NextCustomAddFunction = OpenSelector;
                CallNextDrawer(label);
                if (result == null)
                    return;
                AddResult(result);
                result = null;
                CollectionDrawerStaticInfo.NextCustomAddFunction = customAddFunction;
            }
        }
        else if (Attribute.DrawDropdownForListElements || !isListElement)
            DrawDropdown();
        else
            CallNextDrawer(label);
    }

    private void AddResult(IEnumerable<object> query)
    {
        if (isList)
        {
            IOrderedCollectionResolver childResolver = Property.ChildResolver as IOrderedCollectionResolver;
            if (enableMultiSelect)
                childResolver.QueueClear();
            foreach (object obj in query)
            {
                object[] values = new object[Property.ParentValues.Count];
                for (int index = 0; index < values.Length; ++index)
                    values[index] = obj;
                childResolver.QueueAdd(values);
            }
        }
        else
        {
            object obj = query.FirstOrDefault();
            for (int index = 0; index < Property.ValueEntry.WeakValues.Count; ++index)
                Property.ValueEntry.WeakValues[index] = obj;
        }
    }

    private void DrawDropdown()
    {
        IEnumerable<object> objects;
        if (!isList)
        {
            GUILayout.BeginHorizontal();
            float width = 15f;
            if (label != null)
                width += GUIHelper.BetterLabelWidth;
            objects = OdinSelector<object>.DrawSelectorDropdown(label, GUIContent.none, ShowSelector,
                GUIStyle.none, GUILayoutOptions.Width(width));
            if (Event.current.type == EventType.Repaint)
            {
                Rect position = GUILayoutUtility.GetLastRect().AlignRight(15f);
                position.y += 4f;
                SirenixGUIStyles.PaneOptions.Draw(position, GUIContent.none, 0);
            }

            GUILayout.BeginVertical();
            CallNextDrawer(null);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
        else
            objects = OdinSelector<object>.DrawSelectorDropdown(label, GetCurrentValueName(), ShowSelector);

        if (objects == null || !objects.Any())
            return;
        AddResult(objects);
    }

    private void OpenSelector() => ShowSelector(new Rect(Event.current.mousePosition, Vector2.zero)).SelectionConfirmed +=
        (Action<IEnumerable<object>>) (x => result = x);

    private OdinSelector<object> ShowSelector(Rect rect)
    {
        GenericSelector<object> selector = CreateSelector();
        rect.x = (int) rect.x;
        rect.y = (int) rect.y;
        rect.width = (int) rect.width;
        rect.height = (int) rect.height;
        if (!isList)
            rect.xMax = GUIHelper.GetCurrentLayoutRect().xMax;
        selector.ShowInPopup(rect, new Vector2(Attribute.DropdownWidth, Attribute.DropdownHeight));
        return selector;
    }

    private GenericSelector<object> CreateSelector()
    {
        Attribute.IsUniqueList = Attribute.IsUniqueList || Attribute.ExcludeExistingValuesInList;
        IEnumerable<ValueDropdownItem> source = getValues() ?? Enumerable.Empty<ValueDropdownItem>();
        
        if (source.Any() && (isList && Attribute.ExcludeExistingValuesInList || isListElement && Attribute.IsUniqueList))
        {
            List<ValueDropdownItem> list = source.ToList();
            InspectorProperty parent = Property.FindParent(x => x.ChildResolver is IOrderedCollectionResolver, true);
            ITypesDropdownEqualityComparer comparer = new ITypesDropdownEqualityComparer(false);
            parent.ValueEntry.WeakValues.Cast<IEnumerable>().SelectMany(x => x.Cast<object>())
                .ForEach(x => list.RemoveAll(c => comparer.Equals(c, x)));
            source = list;
        }

        GenericSelector<object> genericSelector = new GenericSelector<object>(Attribute.DropdownTitle, false,
            source.Select(
                x => new GenericSelectorItem<object>(x.Text, x.Value)));
        enableMultiSelect = isList && Attribute.IsUniqueList && !Attribute.ExcludeExistingValuesInList;
        if (Attribute.FlattenTreeView)
            genericSelector.FlattenedTree = true;
        if (isList && !Attribute.ExcludeExistingValuesInList && Attribute.IsUniqueList)
            genericSelector.CheckboxToggle = true;
        else if (!enableMultiSelect)
            genericSelector.EnableSingleClickToSelect();
        if (isList && enableMultiSelect)
        {
            genericSelector.SelectionTree.Selection.SupportsMultiSelect = true;
            genericSelector.DrawConfirmSelectionButton = true;
        }

        genericSelector.SelectionTree.Config.DrawSearchToolbar = true;
        IEnumerable<object> selection = Enumerable.Empty<object>();
        if (!isList)
            selection = getSelection();
        else if (enableMultiSelect)
            selection = getSelection().SelectMany(x => (x as IEnumerable).Cast<object>());
        genericSelector.SetSelection(selection);
        genericSelector.SelectionTree.EnumerateTree().AddThumbnailIcons(true);
        if (Attribute.ExpandAllMenuItems)
            genericSelector.SelectionTree.EnumerateTree(x => x.Toggled = true);
        return genericSelector;
    }

    private string GetCurrentValueName() => !EditorGUI.showMixedValue ? string.Concat(Property.ValueEntry.WeakSmartValue) : "—";
}