﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector.Editor.Drawers;
using Sirenix.Serialization;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

internal class ITypesDropdownEqualityComparer : IEqualityComparer<object>
{
    private bool isTypeLookup;

    public ITypesDropdownEqualityComparer(bool isTypeLookup) => this.isTypeLookup = isTypeLookup;

    public new bool Equals(object x, object y)
    {
        if (x is ValueDropdownItem)
            x = ((ValueDropdownItem) x).Value;
        if (y is ValueDropdownItem)
            y = ((ValueDropdownItem) y).Value;
        if (EqualityComparer<object>.Default.Equals(x, y))
            return true;
        if (x == null && y == null && isTypeLookup)
            return false;
        if (x == null != (y == null) || !isTypeLookup)
            return false;

        var xType = x.GetType();
        var yType = y.GetType();

        var soType = typeof(Object);
        var isBothScriptableObjects = soType.IsAssignableFrom(xType) && soType.IsAssignableFrom(yType);

        if (isBothScriptableObjects)
        {
            return x == y;
        }
        
        if (!(x is Type type))
            type = x.GetType();
        var type1 = type;
        if (!(y is Type type2))
            type2 = y.GetType();
        var type3 = type2;
        return type1 == type3;
    }

    public int GetHashCode(object obj)
    {
        if (obj == null)
            return -1;
        if (obj is ValueDropdownItem)
            obj = ((ValueDropdownItem) obj).Value;
        if (obj == null)
            return -1;
        if (!isTypeLookup)
            return obj.GetHashCode();
        if (!(obj is Type type))
            type = obj.GetType();
        return type.GetHashCode();
    }
}

[DrawerPriority(0.0, 0.0, 3002.0)]
public sealed class TypesDropdownAttributeDrawer : OdinAttributeDrawer<TypesDropdownAttribute>
{
    private string error;
    private GUIContent label;
    private bool isList;
    private bool isListElement;
    private Func<IEnumerable<ValueDropdownItem>> getValues;
    private Func<IEnumerable<object>> getSelection;
    private IEnumerable<object> result;
    private bool enableMultiSelect;
    private Dictionary<object, string> nameLookup;

    private LocalPersistentContext<bool> isToggled;

    private Type BaseType;


    /// <summary>Initializes this instance.</summary>
    protected override void Initialize()
    {
        BaseType = Attribute.TypeOverride ?? Property.Info.TypeOfValue;

        if (EditorUtilities.IsEnumerableType(BaseType) && !BaseType.IsArray)
        {
            BaseType = BaseType.GetGenericArguments().Single() ?? BaseType;
        }

        if (BaseType.IsArray)
        {
            BaseType = BaseType.GetElementType();
        }


        isToggled = this.GetPersistentValue("Toggled", SirenixEditorGUI.ExpandFoldoutByDefault);

        error = null;
        isList = Property.ChildResolver is ICollectionResolver;
        isListElement = Property.Parent != null && Property.Parent.ChildResolver is ICollectionResolver;
        getSelection = () => Property.ValueEntry.WeakValues.Cast<object>();
        getValues = () =>
        {
            var obj = EditorUtilities.AllDerivedTypesDropdownItems(BaseType, Attribute.EnablePresets, Attribute.PresetsPath);
            return obj != null
                ? obj.Cast<object>().Where(x => x != null).Select(x =>
                {
                    switch (x)
                    {
                        case ValueDropdownItem valueDropdownItem2:
                            return valueDropdownItem2;
                        case IValueDropdownItem _:
                            var valueDropdownItem1 = x as IValueDropdownItem;
                            return new ValueDropdownItem(valueDropdownItem1.GetText(), valueDropdownItem1.GetValue());
                        default:
                            return new ValueDropdownItem(null, x);
                    }
                })
                : (IEnumerable<ValueDropdownItem>) null;
        };
        ReloadDropdownCollections();
    }

    private void ReloadDropdownCollections()
    {
        if (error != null)
            return;
        object obj1 = null;
        var obj2 = EditorUtilities.AllDerivedTypesDropdownItems(BaseType, Attribute.EnablePresets, Attribute.PresetsPath);
        if (obj2 != null)
            obj1 = obj2.Cast<object>().FirstOrDefault();
        if (obj1 is IValueDropdownItem)
        {
            var valueDropdownItems = getValues();
            nameLookup = new Dictionary<object, string>(new ITypesDropdownEqualityComparer(true));
            foreach (var valueDropdownItem in valueDropdownItems)
            {
                nameLookup[valueDropdownItem] = valueDropdownItem.Text;
            }
        }
        else
            nameLookup = null;
    }

    private static IEnumerable<ValueDropdownItem> ToValueDropdowns(
        IEnumerable<object> query)
    {
        return query.Select(x =>
        {
            switch (x)
            {
                case ValueDropdownItem valueDropdownItem2:
                    return valueDropdownItem2;
                case IValueDropdownItem _:
                    var valueDropdownItem1 = x as IValueDropdownItem;
                    return new ValueDropdownItem(valueDropdownItem1.GetText(), valueDropdownItem1.GetValue());
                default:
                    return new ValueDropdownItem(null, x);
            }
        });
    }

    /// <summary>
    /// Draws the property with GUILayout support. This method is called by DrawPropertyImplementation if the GUICallType is set to GUILayout, which is the default.
    /// </summary>
    protected override void DrawPropertyLayout(GUIContent label)
    {
        this.label = label;
        if (Property.ValueEntry == null)
        {
            CallNextDrawer(label);
        }
        else if (error != null)
        {
            SirenixEditorGUI.ErrorMessageBox(error);
            CallNextDrawer(label);
        }
        else if (isList)
        {
            if (Attribute.DisableListAddButtonBehaviour)
            {
                CallNextDrawer(label);
            }
            else
            {
                var customAddFunction = CollectionDrawerStaticInfo.NextCustomAddFunction;
                CollectionDrawerStaticInfo.NextCustomAddFunction = OpenSelector;
                CallNextDrawer(label);
                if (result != null)
                {
                    AddResult(result);
                    result = null;
                }

                CollectionDrawerStaticInfo.NextCustomAddFunction = customAddFunction;
            }
        }
        else if (Attribute.DrawDropdownForListElements || !isListElement)
        {
            DrawDropdown();
            // TODO: Доделать Inspect Object для presets
            var soType = typeof(ScriptableObject);
            if (soType.IsAssignableFrom(Property.ValueEntry.TypeOfValue))
            {
                var rect = GUIHelper.GetCurrentLayoutRect();
                //rect.x += EditorGUIUtility.labelWidth - 14f;
                rect.x += rect.width - 44f;
                rect.y += 2f;
                rect.width = rect.height = 20f;
                if (SirenixEditorGUI.IconButton(rect, EditorIcons.Pen, "Inspect object"))
                {
                    var unityObject = Property.ValueEntry.WeakSmartValue as Object;
                    GUIHelper.OpenInspectorWindow(unityObject);
                }
                
            }
        }
        else
        {
            CallNextDrawer(label);
        }

    }

    private void AddResult(IEnumerable<object> query)
    {
        if (isList)
        {
            var childResolver = Property.ChildResolver as ICollectionResolver;
            if (enableMultiSelect)
                childResolver.QueueClear();
            foreach (var obj in query)
            {
                var values = new object[Property.ParentValues.Count];
                for (var index = 0; index < values.Length; ++index)
                    values[index] = Sirenix.Serialization.SerializationUtility.CreateCopy(obj);
                childResolver.QueueAdd(values);
            }
        }
        else
        {
            var obj = query.FirstOrDefault();
            for (var index = 0; index < Property.ValueEntry.WeakValues.Count; ++index)
                Property.ValueEntry.WeakValues[index] = Sirenix.Serialization.SerializationUtility.CreateCopy(obj);
        }
    }

    private void DrawDropdown()
    {
        IEnumerable<object> objects;
        if (Attribute.AppendNextDrawer && !isList)
        {
            GUILayout.BeginHorizontal();
            var width = 15f;
            if (label != null)
                width += GUIHelper.BetterLabelWidth;
            objects = OdinSelector<object>.DrawSelectorDropdown(label, GUIContent.none, ShowSelector, GUIStyle.none, GUILayoutOptions.Width(width));
            if (Event.current.type == EventType.Repaint)
            {
                var position = GUILayoutUtility.GetLastRect().AlignRight(15f);
                position.y += 4f;
                SirenixGUIStyles.PaneOptions.Draw(position, GUIContent.none, 0);
            }

            GUILayout.BeginVertical();
            var inAppendedDrawer = Attribute.DisableGUIInAppendedDrawer;
            if (inAppendedDrawer)
                GUIHelper.PushGUIEnabled(false);
            CallNextDrawer(null);
            if (inAppendedDrawer)
                GUIHelper.PopGUIEnabled();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
        else
        {
            var currentValueName = GetCurrentValueName();
            if (!Attribute.HideChildProperties && Property.Children.Count > 0)
            {
                //Rect valueRect;
                //isToggled.Value = SirenixEditorGUI.Foldout(isToggled.Value, label, out valueRect);
                //SirenixEditorGUI.Foldout(isToggled.Value, label, out valueRect);
                objects = OdinSelector<object>.DrawSelectorDropdown(label, currentValueName, ShowSelector);
                if (SirenixEditorGUI.BeginFadeGroup(this, true))
                {
                    ++EditorGUI.indentLevel;
                    for (var index = 0; index < Property.Children.Count; ++index)
                    {
                        var child = Property.Children[index];
                        child.Draw(child.Label);
                    }
                    --EditorGUI.indentLevel;
                }
                SirenixEditorGUI.EndFadeGroup();
            }
            else
                objects = OdinSelector<object>.DrawSelectorDropdown(label, currentValueName, ShowSelector);
        }

        if (objects == null || !objects.Any())
            return;
        AddResult(objects);
    }

    private void OpenSelector()
    {
        ReloadDropdownCollections();
        ShowSelector(new Rect(Event.current.mousePosition, Vector2.zero)).SelectionConfirmed += (Action<IEnumerable<object>>) (x => result = x);
    }

    private OdinSelector<object> ShowSelector(Rect rect)
    {
        var selector = CreateSelector();
        rect.x = (int) rect.x;
        rect.y = (int) rect.y;
        rect.width = (int) rect.width;
        rect.height = (int) rect.height;
        if (Attribute.AppendNextDrawer && !isList)
            rect.xMax = GUIHelper.GetCurrentLayoutRect().xMax;
        selector.ShowInPopup(rect, new Vector2(Attribute.DropdownWidth, Attribute.DropdownHeight));
        return selector;
    }

    private GenericSelector<object> CreateSelector()
    {
        Attribute.IsUniqueList = !(Property.ChildResolver is IOrderedCollectionResolver) || (Attribute.IsUniqueList || Attribute.ExcludeExistingValuesInList);
        var source = getValues() ?? Enumerable.Empty<ValueDropdownItem>();
        if (source.Any())
        {
            if (isList && Attribute.ExcludeExistingValuesInList || isListElement && Attribute.IsUniqueList)
            {
                var list = source.ToList();
                var parent = Property.FindParent(x => x.ChildResolver is ICollectionResolver, true);
                var comparer = new ITypesDropdownEqualityComparer(true);
                parent.ValueEntry.WeakValues.Cast<IEnumerable>().SelectMany(x => x.Cast<object>()).ForEach(x => list.RemoveAll(c => comparer.Equals(c, x)));
                source = list;
            }

            if (nameLookup != null)
            {
                foreach (var valueDropdownItem in source)
                {
                    if (valueDropdownItem.Value != null)
                        nameLookup[valueDropdownItem.Value] = valueDropdownItem.Text;
                }
            }
        }

        var flag = Attribute.NumberOfItemsBeforeEnablingSearch == 0 || source != null &&
            source.Take(Attribute.NumberOfItemsBeforeEnablingSearch).Count() == Attribute.NumberOfItemsBeforeEnablingSearch;
        var genericSelector = new GenericSelector<object>(Attribute.DropdownTitle, false, source.Select(x => new GenericSelectorItem<object>(x.Text, x.Value)));
        enableMultiSelect = isList && Attribute.IsUniqueList && !Attribute.ExcludeExistingValuesInList;
        if (Attribute.FlattenTreeView)
            genericSelector.FlattenedTree = true;
        if (isList && !Attribute.ExcludeExistingValuesInList && Attribute.IsUniqueList)
            genericSelector.CheckboxToggle = true;
        else if (!Attribute.DoubleClickToConfirm && !enableMultiSelect)
            genericSelector.EnableSingleClickToSelect();
        if (isList && enableMultiSelect)
        {
            genericSelector.SelectionTree.Selection.SupportsMultiSelect = true;
            genericSelector.DrawConfirmSelectionButton = true;
        }

        genericSelector.SelectionTree.Config.DrawSearchToolbar = flag;
        var selection = Enumerable.Empty<object>();
        if (!isList)
            selection = getSelection();
        else if (enableMultiSelect)
            selection = getSelection().SelectMany(x => (x as IEnumerable).Cast<object>());
        genericSelector.SetSelection(selection);
        genericSelector.SelectionTree.EnumerateTree().AddThumbnailIcons(true);
        if (Attribute.ExpandAllMenuItems)
            genericSelector.SelectionTree.EnumerateTree(x => x.Toggled = true);
        if (Attribute.SortDropdownItems)
            genericSelector.SelectionTree.SortMenuItemsByName();
        return genericSelector;
    }

    private string GetCurrentValueName()
    {
        if (EditorGUI.showMixedValue)
            return "—";
        var weakSmartValue = Property.ValueEntry.WeakSmartValue;
        string name = null;
        if (nameLookup != null && weakSmartValue != null)
            nameLookup.TryGetValue(weakSmartValue, out name);

        return new GenericSelectorItem<object>(name, weakSmartValue).GetNiceName();
    }
}